inline size_t Tools::asNumber(Date const &date)
{
    return asNumber(date.year, date.month);
}
