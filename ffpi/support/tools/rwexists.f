inline bool Tools::rwExists(std::string const &fname)
{
    namespace fs = std::filesystem;

    static const fs::perms required = 
                    fs::perms::owner_read | fs::perms::owner_write;

    return (fs::status(fname).permissions() & required) == required;
}
