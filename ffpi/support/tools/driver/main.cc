#include <iostream>
#include <string>
#include <sstream>

#include <bobcat/digestbuf>
#include <bobcat/ibase64stream>
#include <bobcat/ohexbuf>
#include "../tools.h"

using namespace std;
using namespace FBB;

string b64unhash(string &text)
{
    size_t pos = 0;
    while (true)
    {
        pos = text.find('_', pos);           // reset _ to =
        if (pos == string::npos)
            break;
        text[pos++] = '=';
    }

    istringstream in{ text };
    IBase64Stream<DECRYPT> b64{ in, 100 };  // 100 chars for the buffer should
                                            // be OK for e-mail addresses

    ostringstream out;
    out << b64.rdbuf();
    return out.str();
}


int main(int argc, char **argv)
try
{
    DigestBuf db{"md5"};
    ostream out{ &db };
    out << argv[1] << eoi;

    ostringstream hashText;
    hashText << db;

    cout <<hashText.str().substr(0, string::npos) << endl;

return 0;

    string iv = Tools::iv();

    string enc = Tools::encrypt(iv, argv[1]);

cout << enc.length() << '\n';

    
    string b64 = Tools::b64hash(iv + enc);

    cout << b64.length() << ": " << b64 << '\n';


    b64 = b64unhash(b64);
    string iv2{ b64.substr(0, Enum::IV_SIZE) };

    cout << (iv2 == iv2) << endl;

    string addr = Tools::decrypt(iv2, b64.substr(Enum::IV_SIZE));

    cout << addr << '\n';
}
catch (...)
{
    return 1;
}
