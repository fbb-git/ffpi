#include "tools.ih"

// static
std::string Tools::iv()
{
    string ret(Enum::IV_SIZE, '\0');

    for (size_t idx = 0; idx != Enum::IV_SIZE; ++idx)
        ret[idx] = random(255);

    return ret;
}
