#include "tools.ih"

// static 
string Tools::link(string const &linkReference)
{
    ostringstream out;

    out << g_config.findKeyTail("url:") << "/fbin/form?" << linkReference;
    return out.str();
}
