inline size_t Tools::asNumber(size_t year, size_t month)
{
    return (year << 8) + month;
}
