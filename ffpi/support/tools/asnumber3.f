inline size_t Tools::asNumber(FBB::DateTime const &dt)
{
    return asNumber(dt.year(), dt.month());
}
