#include "tools.ih"

// static
void Tools::chdir(string const &dirName)
{
    if (::chdir(&dirName.front()) != 0)             // chdir fails
        throw Exception{} << "can't chdir to " << dirName;
}
