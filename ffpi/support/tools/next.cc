#include "tools.ih"

// static
Date Tools::next(Date const &date)
{
    return normalize(date.year, date.month + 1);
}
