#include "tools.ih"

// static
Date Tools::normalize(size_t year, size_t month)
{
    return Date{ year + month / 12, month % 12 };
}
