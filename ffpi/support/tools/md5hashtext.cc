#include "tools.ih"

    // 16 bytes -> 32 characters
string Tools::md5hashText(string const &text)
{
    DigestBuf digestbuf{ "md5" };
    ostream out{ &digestbuf };

    out << text << eoi;

    ostringstream hashText;
    hashText << digestbuf;

    return hashText.str().substr(0, 16);
}
