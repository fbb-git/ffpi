#ifndef INCLUDED_TOOLS_
#define INCLUDED_TOOLS_

#include <iosfwd>
#include <fstream>
#include <algorithm>
#include <filesystem>

#include <bobcat/datetime>

#include "../types/enum.h"
#include "../types/types.h"

namespace FBB
{
    class CGI;
}

class EnDeCrypt;

class Tools
{
    static EnDeCrypt s_crypt;

    public:
        static void textHtml();
    
        static std::string encrypt(std::string const &iv, 
                                   std::string const &data);
        static std::string decrypt(std::string const &iv, 
                                   std::string const &data);

        static std::string iv();        // return an initialization vector
    
        static bool writeS(std::ostream &out, std::string const &str);  // .f
    
        template<typename Type>                                         // .f
        static bool writeN(std::ostream &out, 
                        Type const *src, size_t nToWrite = 1);
    
                          
                                                    // true: exists for R/W
        static bool rwExists(std::string const &fname);
    
                                                    // resizes dest to nToRead
        static size_t readS(std::istream &in, std::string &dest,
                                              size_t nToRead);
    
        template <typename Type>        // returns the # read Type objects
        static size_t readN(std::istream &in,                           // .f
                         Type *dest, size_t nToRead = 1);
    
    
        static size_t random(int max);  // next random nr from uniform 
                                        // int distribution [0 .. max)
    
                                        // return the 16-byte MD5 hash as 
                                        // displayable text string
        static std::string md5hashText(std::string const &text);
    
        static void childProcess(std::string const &command); // throws false
                                                              // on failure

        static void chdir(std::string const &dirName);          
    
            // month: 0..11, year: actual year (e.g., 2020)
        static size_t asNumber(size_t year, size_t month);      // 1.f
        static size_t asNumber(Date const &date);               // 2.f
        static size_t asNumber(FBB::DateTime const &dt);        // 3.f
    
        static Date normalize(size_t year, size_t month);       // .cc
        static Date next(Date const &date);                     // .cc
    
    private:
        static std::string key();
};

#include "readn.f"
#include "rwexists.f"
#include "writen.f"
#include "writes.f"
#include "asnumber1.f"
#include "asnumber2.f"
#include "asnumber3.f"

#endif
