#include "ratings.ih"

Ratings::Ratings(char const *filename)
{
                                                    // open the CSV file
    ifstream in{ Exception::factory<ifstream>(filename) };

    CSV4180 csv;

    csv.read(in);                                   // read required # lines

    convert(csv.release());
}
