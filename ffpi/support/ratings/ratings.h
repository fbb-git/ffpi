#ifndef INCLUDED_RATINGS_
#define INCLUDED_RATINGS_

#include <string>
#include <vector>

// Scores are received as ascii char. '0' thru '4' for resp. no thru yes
// With std. ratings there are 4 lines: self and 3 other data lines
// convert() reads the data linewise into vector<double>, one vector per line,
// and the vectors are stored into d_data, as values 0 thru 4.

// The Ratings are thereupon procesed by Fscores.

class Ratings
{
    std::vector< std::vector<double> > d_data;

    public:
        Ratings(char const *filename);
        std::vector< std::vector<double> > const &data() const;

    private:
        void convert(std::vector< std::vector<std::string> > const &data);
};

inline std::vector< std::vector<double> > const &Ratings::data() const
{
    return d_data;
}
        
#endif
