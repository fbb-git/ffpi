#include "ratings.ih"

void Ratings::convert(vector< vector<string> > const &data)
{
    unsigned line = 1;
    for (auto const &row: data)
    {
        vector<double> next;

        for (auto const &value: row)
            next.push_back(stod(value));

        if (next.size() != Enum::N_QUESTIONS)
            throw Exception{} << "Data corrupt: line " << line <<
                    " has " << next.size() << " values instead of " <<
                    Enum::N_QUESTIONS;

        d_data.push_back(move(next));
        ++line;
    }
}

