#include "mailer.ih"

std::string Mailer::sendmail(string to, string const &subject,    
                             string const &txt, bool viaChild)
{
    string mailRequest = g_config.findKeyTail("mail:");

    ostringstream log;

    if (mailRequest == "log")
    {
        log << "would have sent mail to `" << to << "', subject: `" << 
                    subject << "', contents: `" << txt << '\'';
        return log.str(); 
    }

    if (mailRequest == "off")
    {
        log << " mail to " << to << " not sent: mailRequest = " << 
                        mailRequest;
        return log.str();
    }

    if (not mailRequest.empty())
        to = mailRequest;

    log << "MAIL is sent to " << mailRequest << '\n';
    log << "     subject: " << subject << '\n';

    d_txt = txt;
    d_to = to;
    d_subject = subject;

    if (viaChild)
        fork();
    else
        mailInserter();

    return log.str();
}






