#ifndef INCLUDED_MAILER_
#define INCLUDED_MAILER_

#include <iosfwd>
#include <string>

#include <bobcat/fork>

class Mailer: private FBB::Fork
{
    std::string d_txt;
    std::string d_to;
    std::string d_subject;

    public:
                                        // returns a log message
        std::string sendmail(std::string to, 
                             std::string const &subject, 
                             std::string const &txt,
                             bool viaChild = true); // Reporter uses false
                                                    // and calls mailInserter
    private:
        void childProcess() override;       // calls mailInserter as child
        void parentProcess() override;

        void mailInserter() const;          // uses CinInserter to send mail

};

extern Mailer g_mailer;

// Sending e-mail is handled by childProcess, using CinInserter receiving 
// the text passed to the member sendmail. Since all activities by the Mailer
// except for actually sending mail is handled at the form-program level, all
// log messages can directly be written by lg(), and any messages generated by
// the childProcess can be ignored.

#endif
