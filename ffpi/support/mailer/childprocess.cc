#include "mailer.ih"

void Mailer::childProcess()
{
    prepareDaemon();

    mailInserter();

    throw Enum::ForkChild::DONE;
}
