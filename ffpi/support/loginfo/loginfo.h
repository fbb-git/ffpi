#ifndef INCLUDED_LOGINFO_
#define INCLUDED_LOGINFO_

#include <sstream>

#include <bobcat/log>
#include <bobcat/level>

#include "../types/enum.h"

class LogInfo
{
    std::ostringstream  d_logTxt;
    FBB::Log            d_log;
    Enum::LogLevel      d_logLevel = Enum::INFO;

    static std::string s_logLevels[];
    static std::string *const s_logLevelsEnd;

    public:
        LogInfo(char const *programName);
        ~LogInfo();                     // flushes the logs

        void setLevel();                // uses g_config and g_options

        std::ostream &log(Enum::LogLevel value);
        Enum::LogLevel level() const;
};

extern LogInfo g_logInfo;

inline Enum::LogLevel LogInfo::level() const
{
    return d_logLevel;
}

inline std::ostream &debug()
{
    return g_logInfo.log(Enum::DEBUG);
}

inline std::ostream &info()
{
    return g_logInfo.log(Enum::INFO);
}

inline std::ostream &warn()
{
    return g_logInfo.log(Enum::WARN);
}

inline std::ostream &err()
{
    return g_logInfo.log(Enum::ERR);
}

inline std::ostream &crit()
{
    return g_logInfo.log(Enum::CRIT);
}

#endif


