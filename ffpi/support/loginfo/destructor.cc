#include "loginfo.ih"

LogInfo::~LogInfo()
{
    string txt = d_logTxt.str();

    if (txt.empty())
        return;

    int fd = open(g_options.log().c_str(), O_WRONLY);   // lock the logfile
    flock(fd, LOCK_EX);    

    try
    {
        Exception::factory<ofstream>(g_options.log(), ios::in | ios::ate) <<
                    txt << '\n';
    }
    catch (...)
    {
        cerr << "~LogInfo failed: " << txt << '\n';
    }
}
