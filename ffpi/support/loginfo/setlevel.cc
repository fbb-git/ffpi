//#define XERR
#include "loginfo.ih"

void LogInfo::setLevel()
{
    string value{ g_config.findKeyTail("log:") };   // get the log level
    try                                             // option('L') is not 
    {                                               //  required
        Arg::instance().option(&value, 'L');        // get the option or use 
    }
    catch (...)
    {}

                                            // find the specified log level
    auto iter = find(s_logLevels, s_logLevelsEnd, value);

    if (iter == s_logLevelsEnd)             // no or unknown log specification
    {
        d_log.off();                        // no type: nothing is logged
        d_logLevel = Enum::LOG_OFF;
        return;
    }

    d_logLevel = static_cast<Enum::LogLevel>(iter - s_logLevels);
    d_log.setLevel(d_logLevel);              // set the message threshold
}    



