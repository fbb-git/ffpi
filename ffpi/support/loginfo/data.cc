//#define XERR
#include "loginfo.ih"

string LogInfo::s_logLevels[] =
        {
            "debug",                // must correspond with
            "info",                 // Enum::LogLevel
            "warn",
            "err",
            "crit",
        };

string *const LogInfo::s_logLevelsEnd = LogInfo::s_logLevels + 
                                                size(LogInfo::s_logLevels);

