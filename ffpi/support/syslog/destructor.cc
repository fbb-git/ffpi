#define XERR
#include "syslog.ih"

Syslog::~Syslog()
{
    if (d_msg->str().empty())
        return;

    s_sls << d_msg->str() << endl;
    d_msg->str("");
}
