#include <iostream>
//#include <string>

#include "../syslog.h"

using namespace std;
using namespace FBB;

SyslogStream Syslog::s_sls{ "driver" };

int main()
try
{
    throw Syslog{ Enum::CHILD } << "child process" << " ...whatever... " <<
                                    "failed";
}
catch (Syslog const &sl)
{
    cout << "got syslog value " << sl.type() << '\n';
}
catch (...)
{
    return 1;
}
