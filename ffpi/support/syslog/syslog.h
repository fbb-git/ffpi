#ifndef INCLUDED_SYSLOG_
#define INCLUDED_SYSLOG_


#include <memory>
#include <sstream>
#include <bobcat/syslogstream>

#include "../types/enum.h"

class Syslog
{
    template<typename Type>
    friend Syslog &&operator<<(Syslog &&syslog, Type const &value);
    
    static FBB::SyslogStream s_sls;         // defined in main, providing
                                            // the program's name.
            // define as, e.g.,
            // FBB::SyslogStream Syslog::s_sls{ "programname" };, 
            //  which by default logs to the USER facility

    std::shared_ptr<std::ostringstream> d_msg;
    Enum::Error d_type;

    public:
        Syslog(Enum::Error type);
        ~Syslog();

        Enum::Error type() const;

        static FBB::SyslogStream &log();
};
        
inline Enum::Error Syslog::type() const
{
    return d_type;
}

template<typename Type>
Syslog &&operator<<(Syslog &&syslog, Type const &value)
{
    *syslog.d_msg << value;
    return std::move(syslog);
}

inline FBB::SyslogStream &Syslog::log()
{
    return s_sls;
}


#endif



