#ifndef INCLUDED_OPTIONS_
#define INCLUDED_OPTIONS_

#include <string>

class Options
{
    std::string d_base;                 // the 'active' directory
//  static std::string s_msg;           // SyslogStream msg at Enum::NO_DATA

    public:
        Options(std::string const &base);

        std::string basePath() const;   // all Dir-entries end in '/'

        std::string htmlPath() const;    

        std::string researchPath() const;

        std::string dataPath() const;    
        std::string dataDir() const;    
        std::string mailPath() const;    
        std::string mailDir() const;    
        std::string config() const;
        std::string log() const;
        std::string tmpPath() const;     
        std::string tmpDir() const;     
        std::string moldsDir() const;   
        std::string reportsDir() const; 
        std::string binDir() const;     
};

inline std::string Options::basePath() const
{
    return d_base;
}

inline std::string Options::tmpDir() const
{
    return "tmp/";
}

inline std::string Options::tmpPath() const
{
    return d_base + "tmp/";
}

inline std::string Options::researchPath() const
{
    return d_base + "research/";
}

inline std::string Options::dataPath() const
{
    return d_base + "data/";
}

inline std::string Options::dataDir() const
{
    return "data/";
}

inline std::string Options::htmlPath() const
{
    return d_base + "html/";
}

inline std::string Options::mailPath() const
{
    return d_base + "mail/";
}

inline std::string Options::mailDir() const
{
    return "mail/";
}

inline std::string Options::config() const
{
    return d_base + "etc/ffpi_config";
}

inline std::string Options::log() const
{
    return d_base + "log";
}

inline std::string Options::moldsDir() const
{
    return "molds/";
}

inline std::string Options::reportsDir() const
{
    return "reports/";
}

inline std::string Options::binDir() const
{
    return "bin/";
}

extern Options g_options;

#endif
