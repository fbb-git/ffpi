#ifndef INCLUDED_ENDECRYPT_
#define INCLUDED_ENDECRYPT_

#include <iosfwd>
#include <vector>

class EnDeCrypt
{
    std::vector<unsigned short> d_key;

    public:
        EnDeCrypt();

        std::string process(std::string const &in);
};
        
#endif
