#include "datastore.ih"

    // write a file in the data directory named 'fname'

void DataStore::write(std::string const &fname, string const &data)
{
    info() << __FILE__ << ": write " << fname << endl;

                                            // create/rewrite the psych file
    ofstream user{ Exception::factory<ofstream>(d_data + fname) };   
    Tools::writeS(user, data);              // write the data
}


