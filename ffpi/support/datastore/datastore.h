#ifndef INCLUDED_DATASTORE_
#define INCLUDED_DATASTORE_

#include <string>

// keys must be Enum::KEY_SIZE bytes. NOT checked!

class DataStore
{
    std::string d_data;                 // path to {webbase}/data/

    static char const s_nextNr[];       // nextnr file, in {webbase}/data/

    public:
        DataStore() = default;          // not used by form, but by, e.g.,
                                        // psychrm to create a plain Psych
                                        // object.
                                        
        DataStore(std::string const &path);     // full path to the 
                                                // psych data directory

        bool read(std::string *data, std::string const &key);

        void write(std::string const &key, std::string const &data);

        std::string const &path() const;    // path to data/

    private:
        void ensure(char const *name);  // ensure the existence of
                                        // the file d_data/name
};

inline std::string const &DataStore::path() const
{
    return d_data;
}

#endif
