#include "main.ih"

Log m_log{ "log" };
int m_logFD = -1;

int main()
try
{
    DataStore dataStore{ "data" };

    string line;
    while (true)
    {
        LockGuard guard{ dataStore.guard() };

        cout << "? ";
        if (not getline(cin, line))
            return 0;

        istringstream in{ line };
        string cmd;
        in >> cmd;

        switch (cmd[0])
        {
            case 'a':
                in >> cmd;          // key
                getline(in, line);  // value
                if (dataStore.add(Tools::md5hash(cmd), line))
                    cout << "OK\n";
                else
                    cout << "key `" << cmd << "' already defined\n";
            break;

            case 'd':
                in >> cmd;
                if (dataStore.erase(Tools::md5hash(cmd)))
                    cout << "OK\n";
                else
                    cout << "key `" << cmd << "' not found\n";
            break;
                        
            case 'g':
                in >> cmd;
                if (dataStore.get(&line, Tools::md5hash(cmd)))
                    cout << "OK: `" << line << "'\n";
                else
                    cout << "key `" << cmd << "' not found\n";
            break;
                        
            case 'u':
                in >> cmd;          // key
                getline(in, line);  // value
                if (dataStore.update(Tools::md5hash(cmd), line))
                    cout << "OK\n";
                else
                    cout << "key `" << cmd << "' not found\n";
            break;

            case 'q':
                cout << "bye\n";
            return 0;
        
            default:
                cout << "??\n";
            break;
        }
    }
}
catch (exception const &exc)
{
    cout << "Exception: " << exc.what() << '\n';
}
catch (...)
{
    cout << "Unexpected exception\n";
}

