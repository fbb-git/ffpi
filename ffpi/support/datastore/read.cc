#include "datastore.ih"

bool DataStore::read(string *data, string const &key)
try
{
    info() << __FILE__ << ": read " << key << endl;

    string path{ d_data + key };

    Stat stat{ path };

    if (not stat)
        return false;

    ifstream in{ Exception::factory<ifstream>(path) };

    Tools::readS(in, *data, stat.size());

    return true;
}
catch (...)
{
    return false;
}

