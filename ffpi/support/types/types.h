#ifndef INCLUDED_TYPES_H_
#define INCLUDED_TYPES_H_

#include <vector>
#include <string>

#include "enum.h"

typedef std::vector<std::string>            StrVector;
typedef std::vector<double>                 Dvector;
typedef std::vector< Dvector >              Dvector2;
typedef std::vector<unsigned>               Uvector;

struct ProjectData
{
    uint32_t d_encryptSize;          // size of encrypted e-mail address
    uint32_t d_projectNr; 
    uint16_t d_flags;
    uint16_t d_year;                 // 2000+ (full year)
    uint8_t  d_month;                // 0..11

    uint16_t encryptSize() const;
    uint32_t projectNr() const;
    uint16_t month() const;
    uint16_t year() const;
    uint16_t flags() const;
};

inline uint16_t ProjectData::encryptSize() const
{
    return d_encryptSize;
}

inline uint32_t ProjectData::projectNr() const
{
    return d_projectNr;
}

inline uint16_t ProjectData::month() const
{
    return d_month;
}

inline uint16_t ProjectData::year() const
{
    return d_year;
}

inline uint16_t ProjectData::flags() const
{
    return d_flags;
}

struct Adjective
{
    double comm;                                // communality
    double norm;                                // sqrt(loadingsSS)
    double loadings[Enum::N_FACTORS];           // loadings
    std::string adjective[Enum::N_LANGUAGES];   // adjective translations 
                                                // (NL, EN)
};

struct Date
{
    size_t year;            // e.g., 2020
    size_t month;           // 0..11
};


#endif




