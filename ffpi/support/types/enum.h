#ifndef INCLUDED_ENUM_H_
#define INCLUDED_ENUM_H_

struct Enum
{
    enum Constants
    {
        N_QUESTIONS = 100,
        N_OTHER = 3,            // assuming 1 x self, 1 x meta rating
        IV_SIZE = 8,
        N_FACTORS = 5,
        N_USE_ADJECTIVES = 3,   // #adjectives to use by Language::adjectives
    };

    static float constexpr MAX_FSCORE =  3.5;
    static float constexpr MIN_FSCORE = -3.0;

    enum Gender
    {
        FEMALE,
        MALE
    };

        // Note: when changing this enum, also update support/loginfo/data.cc
    enum LogLevel               // levels specified for logging, used for 
    {                           // setting g_log's level and the level of
        DEBUG,                  // messages. Message levels >= g_log's level
        INFO,                   // are shown. g_log off: no log messages
        WARN,                   
        ERR,                    // error, usually user error
        CRIT,                   // system error: check this out
                                // EMERG is handled by SyslogStream

        LOG_OFF = 0xff          // indicates no logging.
    };

    enum class ForkChild
    {
        DONE
    };

    enum Error              // cf Language::s_messages
    {
        DATA_AVAILABLE,     
        NO_DATA,
        NO_STATE,
        NO_QUERY,
        CHILD,
        LATEXINPUT,
        INVALID_DATA,

        N_MESSAGES          // not an error message index
    };


    enum Language
    {
        NL,
        EN,

        N_LANGUAGES                 // not a language
    };

    enum RatingType
    {
        STANDARD,                   // self + other (2 ratings)
        SELF,
        IDEAL,

        N_RATINGTYPES               // not a rating type
    };
    
    enum Align
    {
        LEFT,
        RIGHT
    };
};

#endif
