#include "lockguard.ih"

LockGuard::~LockGuard()
{
    if (s_locked[d_path] != 0)
        --s_locked[d_path];

    if (s_locked[d_path] == 0)              // no more lock recursions
    {
        debug() << "unlocked " << d_path << endl;
        flock(d_fd, LOCK_UN);               // unlock, and rm 'path' from
        s_locked.erase(d_path);             // the map of locked streams
    }
}
