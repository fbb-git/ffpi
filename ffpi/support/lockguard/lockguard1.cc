#include "lockguard.ih"

    // repeated use of LockGueard for the same 'path' by the same process is
    // OK: a static map keeps track of the lock-count. So in fact this is
    // a recursive lock

    // the lock file is created when not existing, otherwise it's kept as-is

LockGuard::LockGuard(string const &path)
:
    d_path(path),
    d_fd(::open(path.c_str(), O_CREAT, 0644))
{
    if (d_fd == -1)
    {
        err() << "can't ::open " << path << endl;
        throw Syslog{ Enum::NO_DATA };
    }

    if (s_locked.find(path) == s_locked.end())              // not yet locked?
    {
        if (flock(d_fd, LOCK_EX) == 0)                      // then lock
            debug() << "locked " << path << endl;
        else
        {
            err() << "can't lock " << path << endl;
            throw Enum::NO_DATA;
        }
    }
    ++s_locked[path];
}










