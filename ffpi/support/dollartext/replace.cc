#include "dollartext.ih"

// static
void DollarText::replace(string const &outPath, string const &inPath, 
                         StrVector const &elements)
{
    ofstream out{ Exception::factory<ofstream>(outPath) };
    ifstream in{  Exception::factory<ifstream>(inPath) };

    string line;
    while (getline(in, line))
        out << replaceLine(line, elements);
}
