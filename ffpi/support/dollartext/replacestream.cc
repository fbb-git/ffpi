#include "dollartext.ih"

string DollarText::replaceStream(string const &path, 
                                 StrVector const &elements)
{
    auto in{ Exception::factory<ifstream>(path) };

    string ret;
    string line;

    while (getline(in, line))
        ret += replaceLine(line, elements);

    return ret;
}
