#include "display.ih"

void Display::out(string const &name)
{
    string path = g_options.htmlPath() + g_language.prefix() + name;
    state(path);

    ifstream in;
    Exception::open(in, path);

    cout << in.rdbuf();                 // no variables: file to cout

    end();
}
