inline void Language::setLanguage(std::string const &prefix)
{
    d_language = prefix.empty() ? Enum::NL : Enum::EN;
}
