inline char const *Language::message(Enum::Error enumValue) const
{
    return s_messages[d_language][enumValue];
}
