inline char const *Language::facet(int factor1, int posneg1, 
                                   int factor2, int posneg2) const
{
    return s_facet[d_language][factor1][posneg1][factor2][posneg2];
}
