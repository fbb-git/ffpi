#ifndef INCLUDED_LANGUAGE_
#define INCLUDED_LANGUAGE_

#include <string>
#include "../types/enum.h"
#include "../types/types.h"

// The labels used for generating the scores table by fsplot are defined
// in fsplot/datafiles/data.cc

class Language
{
    Enum::Language d_language;

    static char const *s_prefix[Enum::N_LANGUAGES];
    static char const *s_messages[Enum::N_LANGUAGES][Enum::N_MESSAGES];

                        // max. dim. score, neg/pos, next highest, neg/pos
                        // 0: neg, 1: pos
//    static char const *s_facet
//                [Enum::N_LANGUAGES][Enum::N_FACTORS][2][Enum::N_FACTORS][2];
//
//    static char const *s_factorLabel[Enum::N_LANGUAGES][Enum::N_FACTORS][2];

    static char const *s_sendmailSubject[];

    static char const *s_header[Enum::N_LANGUAGES][2];  // self, ideal

    static char const *s_person[Enum::N_LANGUAGES];

    static char const *s_date[Enum::N_LANGUAGES];

    static char const *s_hisHer[][2];
    static char const *s_himHer[][2];
    static char const *s_heShe[][2];

    static char const *s_close[];

    static char const *s_closeMereSelf[];

    static char const *s_selfThanks[];
    static char const *s_otherThanks[];

    static char const *s_areYou[];
    static char const *s_isIdeal[];

    static char const *s_mailSubject[][3];

    static char const *s_month[][12];

    static Adjective const s_adjective[];
    static size_t s_adjectiveSize;

    public:
        void setLanguage(std::string const &prefix);        // 1.f
        void setLanguage(int idx);                          // 2.f

        uint16_t idx() const;           // the language-index (NL, EN, ...)
        std::string str() const;
        char const *prefix() const;
        char const *message(Enum::Error enumValue) const;
        char const *areYou() const;
        char const *close() const;
        char const *closeMereSelf() const;
        char const *himHer(Enum::Gender gender) const;
        char const *hisHer(Enum::Gender gender) const;
        char const *heShe(Enum::Gender gender) const;
        char const *selfThanks() const;
        char const *otherThanks() const;
        char const *mailSubject(int idx) const;
        char const *sendmailSubject() const;
        char const *isIdeal() const;
//        char const *factorLabel(int factor, int posneg) const;
//        char const *facet(int factor1, int posneg1, 
//                                        int factor2, int posneg2) const;
        char const *person() const;
        char const *date() const;
        char const *header(bool idealType) const;
        char const *month(size_t idx) const;

        std::string adjectives(Uvector const &indices) const; 

        static size_t adjectiveSize();
        static Adjective const *adjective();    // returns s_adjective
};

#include "adjective.f"
#include "adjectivesize.f"
#include "idx.f"
#include "month.f"
#include "str.f"
#include "mailsubject.f"
#include "selfthanks.f"
#include "otherthanks.f"
#include "heshe.f"
#include "hisher.f"
#include "himher.f"
#include "close.f"
#include "closemereself.f"
#include "areyou.f"
#include "isideal.f"
#include "setlanguage1.f"
#include "setlanguage2.f"
#include "prefix.f"
#include "message.f"
// #include "factorlabel.f"
// #include "facet.f"
#include "sendmailsubject.f"
#include "person.f"
#include "date.f"
#include "header.f"

extern Language g_language;
        
#endif
