//#define XERR
#include "main.ih"

Language g_language;

int main(int argc, char **argv)
{
    string line;

    while (true)
    {
        cout << "language: [empty: dutch, e: english]";
        getline(cin, line);

        g_language.setLanguage(line);

        while (true)
        {
            cout << "factor1 (0-4) pos/neg (pn) factor2 (0-4) pos/neg (pn) ?";
            int factor1, factor2;
            char pn1, pn2;
            cin >> factor1 >> pn1 >> factor2 >> pn2;
            pn1 = pn1 == 'p';
            pn2 = pn2 == 'p';
            getline(cin, line);

            cout << g_language.facet(factor1, pn1, factor2, pn2) << '\n';
        }
    }
        
}
