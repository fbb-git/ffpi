//#define XERR
#include "language.ih"

// The labels used for generating the scores table by fsplot are defined
// in fsplot/datafiles/data.cc

char const *Language::s_prefix[Enum::N_LANGUAGES] = 
{
    "",     // NL
    "en/"   // EN
};

    // see also types/enum.h
char const *Language::s_messages[Enum::N_LANGUAGES][Enum::N_MESSAGES] =
{
    "De vragenlijst is al ingevuld",                                    // DATA_AVAILABLE,     
    "Data niet toegankelijk",                                           // NO_DATA,            
    "Niet gedefinieerde opdracht",                                      // NO_STATE,           
    "De gespecificieerde URI is niet beschikbaar",                      // NO_QUERY,           
    "Falende deelopdracht",                                             // CHILD,              
    "Interne fout (indicator error) svp e-mailen aan ffpi@ffpi.nl",     // LATEXINPUT          
    "Onvolledige of incorrect gespecificeerde gegevens",                // INVALID_DATA

    "The questionnaire has already been answered",                      // DATA_AVAILABLE,     
    "Data not accessible",                                              // NO_DATA,            
    "Request not implemented",                                          // NO_STATE,           
    "The specified URI is not available",                               // NO_QUERY,           
    "Command failed",                                                   // CHILD,              
    "Internal error (indicator error) please e-mail to ffpi@ffpi.nl",   // LATEXINPUT          
    "Incomplete or errors in provided data",                            // INVALID_DATA        
};

// char const *Language::s_factorLabel[Enum::N_LANGUAGES][Enum::N_FACTORS][2] =
// {
//     {
//         { "Introvert",      "Extravert"         },      // 0 negative, 1: positive
//         { "Hard",           "Mild"              },
//         { "Slordig",        "Ordelijk"          },
//         { "Instabiel",      "Emotioneel Stabiel"},
//         { "Afhankelijk",    "Autonoom"          },
//     },
//     {
//         { "Introverted",    "Extraverted"       },      // see also en/gp.labels
//         { "Unforgiving",    "Mild"              },
//         { "Sloppy",         "Orderly",          },
//         { "Unstable",       "Emotionally Stable"},
//         { "Dependent",      "Autonomous"        },
//     }
// };

char const *Language::s_header[Enum::N_LANGUAGES][2] = 
{
    "Zelf", "Ideaal",   
    "Self", "Ideal"
};


char const *Language::s_person[Enum::N_LANGUAGES] = 
{
    "Persoon", "Person"
};

char const *Language::s_date[Enum::N_LANGUAGES] = 
{
    "Afnamedatum", "Date"
};

char const *Language::s_sendmailSubject[] = 
{
    "De rapportage behorend bij uw FFPI vragenlijstafname is beschikbaar",
    "The report corresponding to your FFPI questionnaire ratings is available",
};

// char const *Language::s_facet
//                 [Enum::N_LANGUAGES][Enum::N_FACTORS][2][Enum::N_FACTORS][2] =
// {
//                                             // FP: Factor primary
//                                             // FS: Factor secundary
//                                                             // L FP  FS 
//                                                             //   0 - 0 -
//                                                             //       4 +
//     "",                                                     // 0 0 0 0 0
//     "",                                                     // 0 0 0 0 1
//     "stug, ontoegeeflijk, eenzelvig",                       // 0 0 0 1 0 
//     "stil, rustig, bedaard",                                // 0 0 0 1 1 
//     "nihilistisch",                                         // 0 0 0 2 0 
//     "gereserveerd, gematigd, ingetogen",                    // 0 0 0 2 1 
//     "somber, pessimistisch, negatief",                      // 0 0 0 3 0 
//     "sto&iuml,cijns, houdt gedachten voor zich",            // 0 0 0 3 1 
//     "schuw, terughoudend, bedeesd",                         // 0 0 0 4 0 
//     "individualistisch, beschouwelijk",                     // 0 0 0 4 1 
// 
//                                                             //   0 + 0 -
//                                                             //       4 +
//     "",                                                     // 0 0 1 0 0
//     "",                                                     // 0 0 1 0 1
//     "lawaaierig, rumoerig, uitdagend",                      // 0 0 1 1 0 
//     "spontaan, zonnig, blij",                               // 0 0 1 1 1 
//     "uitbundig, kletserig, ongeremd",                       // 0 0 1 2 0 
//     "charmant",                                             // 0 0 1 2 1 
//     "hartstochtelijk, draagt het hart op de tong",          // 0 0 1 3 0 
//     "opgewekt, blijmoedig, vrolijk",                        // 0 0 1 3 1 
//     "praatziek, loslippig",                                 // 0 0 1 4 0 
//     "levendig, enthousiast, vrijmoedig",                    // 0 0 1 4 1 
// 
//                                                             //   1 - 0 -
//                                                             //       4 +
//     "nors, sikkeneurig, kortaangebonden",                   // 0 1 0 0 0 
//     "dominant, bemoeiziek, bedilzuchtig",                   // 0 1 0 0 1 
//     "",                                                     // 0 1 0 1 0
//     "",                                                     // 0 1 0 1 1
//     "dwars, opstandig, temperamentvol",                     // 0 1 0 2 0 
//     "streberisch, veeleisend, streng",                      // 0 1 0 2 1 
//     "driftig, prikkelbaar, ongeduldig",                     // 0 1 0 3 0 
//     "keihard, dictatoriaal, arrogant",                      // 0 1 0 3 1 
//     "onverdraagzaam, onredelijk, hebberig",                 // 0 1 0 4 0 
//     "fel, onverzettelijk, eigengereid",                     // 0 1 0 4 1 
// 
//                                                             //   1 + 0 -
//                                                             //       4 +
//     "bescheiden, laat de eer aan anderen",                  // 0 1 1 0 0 
//     "zorgzaam, ruimhartig, bekommert zich om anderen",      // 0 1 1 0 1 
//     "",                                                     // 0 1 1 1 0
//     "",                                                     // 0 1 1 1 1
//     "plooibaar",                                            // 0 1 1 2 0 
//     "trouwhartig, gedienstig, wellevend",                   // 0 1 1 2 1 
//     "gevoelig, zacht, teerhartig",                          // 0 1 1 3 0 
//     "geduldig, positief, gemoedelijk",                      // 0 1 1 3 1 
//     "zachtmoedig, gewillig, inschikkelijk",                 // 0 1 1 4 0 
//     "rechtvaardig, redelijk, oprecht",                      // 0 1 1 4 1 
// 
//                                                             //   2 - 0 -
//                                                             //       4 +
//     "zonderling, verstrooid, onattent",                     // 0 2 0 0 0 
//     "losbandig, onbesuisd, losbollig",                      // 0 2 0 0 1 
//     "onvoorzichtig, roekeloos, onbezonnen",                 // 0 2 0 1 0 
//     "hecht niet aan plannen",                               // 0 2 0 1 1 
//     "",                                                     // 0 2 0 2 0
//     "",                                                     // 0 2 0 2 1
//     "wispelturig, ongedurig, onbeheerst",                   // 0 2 0 3 0 
//     "denkt niet aan morgen",                                // 0 2 0 3 1 
//     "gemakzuchtig, onverstandig, laks",                     // 0 2 0 4 0 
//     "ondogmatisch, ongehoorzaam, driest",                   // 0 2 0 4 1 
// 
//                                                             //   2 + 0 -
//                                                             //       4 +
//     "degelijk, behoedzaam, bezonnen",                       // 0 2 1 0 0 
//     "besteedt zorg aan dingen",                             // 0 2 1 0 1 
//     "wil dat alles volgens plan verloopt",                  // 0 2 1 1 0 
//     "net, vlijtig, plichtsgetrouw",                         // 0 2 1 1 1 
//     "",                                                     // 0 2 1 2 0
//     "",                                                     // 0 2 1 2 1
//     "houdt van zekerheid",                                  // 0 2 1 3 0 
//     "gelijkmatig, formeel, rechtlijnig",                    // 0 2 1 3 1 
//     "gezagsgetrouw, gehoorzaam, behoudend",                 // 0 2 1 4 0 
//     "zorgvuldig, effici&euml,nt, gedisciplineerd",          // 0 2 1 4 1 
// 
//                                                             //   3 - 0 -
//                                                             //       4 +
//     "zwaarmoedig, neerslachtig, zwaartillend",              // 0 3 0 0 0 
//     "wil graag aardig gevonden worden",                     // 0 3 0 0 1 
//     "lichtgeraakt, kribbig, humeurig",                      // 0 3 0 1 0 
//     "overgevoelig, emotioneel, kwetsbaar",                  // 0 3 0 1 1 
//     "labiel, wisselvallig, irrationeel",                    // 0 3 0 2 0 
//     "maakt zich onnodig zorgen",                            // 0 3 0 2 1 
//     "",                                                     // 0 3 0 3 0
//     "",                                                     // 0 3 0 3 1
//     "paniekerig, onzeker, onevenwichtig",                   // 0 3 0 4 0 
//     "complex, gepassioneerd",                               // 0 3 0 4 1 
// 
//                                                             //   3 + 0 -
//                                                             //       4 +
//     "beheerst, doodkalm, onverstoorbaar",                   // 0 3 1 0 0 
//     "optimistisch, levenskrachtig, ongecompliceerd",        // 0 3 1 0 1 
//     "sluw, zelfvoldaan, gevoelloos",                        // 0 3 1 1 0 
//     "kalm, gelijkmoedig, onbezorgd",                        // 0 3 1 1 1 
//     "zorgeloos, onbekommerd, laconiek",                     // 0 3 1 2 0 
//     "stabiel, evenwichtig, nuchter",                        // 0 3 1 2 1 
//     "",                                                     // 0 3 1 3 0
//     "",                                                     // 0 3 1 3 1
//     "corrupt, glibberig",                                   // 0 3 1 4 0 
//     "zeker, onbevreesd, rationeel",                         // 0 3 1 4 1 
// 
//                                                             //   4 - 0 -
//                                                             //       4 +
//     "passief, slap, willoos",                               // 0 4 0 0 0 
//     "goedhartig, dweperig",                                 // 0 4 0 0 1 
//     "oppervlakkig, kortzichtig, schijnheilig",              // 0 4 0 1 0 
//     "volgzaam, nederig, stemt overal mee in",               // 0 4 0 1 1 
//     "onnozel, sullig, inactief",                            // 0 4 0 2 0 
//     "braaf, alledaags, burgerlijk",                         // 0 4 0 2 1 
//     "weifelachtig, afhankelijk, besluiteloos",              // 0 4 0 3 0 
//     "onbetrouwbaar, slijmerig",                             // 0 4 0 3 1 
//     "",                                                     // 0 4 0 4 0
//     "",                                                     // 0 4 0 4 1
// 
//                                                             //   4 + 0 -
//                                                             //       4 +
//     "analytisch, sceptisch",                                // 0 4 1 0 0 
//     "strijdvaardig, ondernemend, vindingrijk",              // 0 4 1 0 1 
//     "weet een situatie naar zijn of haar hand te zetten",   // 0 4 1 1 0 
//     "ruimdenkend, diepzinnig, constructief",                // 0 4 1 1 1 
//     "rebels, vrijgevochten, revolutionair",                 // 0 4 1 2 0 
//     "doelbewust, doortastend, besluitvaardig",              // 0 4 1 2 1 
//     "denkt diep over dingen na",                            // 0 4 1 3 0 
//     "vastberaden, resoluut, scherp",                        // 0 4 1 3 1 
//     "",                                                     // 0 4 1 4 0
//     "",                                                     // 0 4 1 4 1
// 
//     // ENGLISH:
//                                                              //   0 - 0 -
//                                                              //       4 +
//      "",                                                     // 1 0 0 0 0
//      "",                                                     // 1 0 0 0 1
//      "unwelcoming, unyielding, reclusive",                   // 1 0 0 1 0
//      "quiet, still, tranquil",                               // 1 0 0 1 1
//      "nihilistic",                                           // 1 0 0 2 0
//      "reserved, moderate, restrained",                       // 1 0 0 2 1
//      "somber, pessimistic, negativistic",                    // 1 0 0 3 0
//      "stoic, keeps his/her thoughts to himself/herself",     // 1 0 0 3 1
//      "shy, reticent, meek",                                  // 1 0 0 4 0
//      "individualistic, philosophical",                       // 1 0 0 4 1
// 
//                                                              //   0 + 0 -
//                                                              //       4 +
//      "",                                                     // 1 0 1 0 0
//      "",                                                     // 1 0 1 0 1
//      "noisy, boisterous, provocative",                       // 1 0 1 1 0
//      "spontaneous, sunny, happy",                            // 1 0 1 1 1
//      "exuberant, chatty, unrestrained",                      // 1 0 1 2 0
//      "charming",                                             // 1 0 1 2 1
//      "impassioned, wears his/her heart on his/her sleeve",   // 1 0 1 3 0
//      "cheerful, merry, joyful",                              // 1 0 1 3 1
//      "gossipy, indiscrete",                                  // 1 0 1 4 0
//      "outgoing, enthusiastic, outspoken",                    // 1 0 1 4 1
// 
//                                                              //   1 - 0 -
//                                                              //       4 +
//      "surly, grumpy, curt",                                  // 1 1 0 0 0
//      "dominant, meddlesome, fault-finding",                  // 1 1 0 0 1
//      "",                                                     // 1 1 0 1 0
//      "",                                                     // 1 1 0 1 1
//      "uncooperative, recalcitrant, hot-headed",              // 1 1 0 2 0
//      "pushy, demanding, stern",                              // 1 1 0 2 1
//      "hot-tempered, irritable, impatient",                   // 1 1 0 3 0
//      "hard-boiled, dictatorial, arrogant",                   // 1 1 0 3 1
//      "intolerant, unreasonable, greedy",                     // 1 1 0 4 0
//      "fiery, stubborn, self-willed",                         // 1 1 0 4 1
// 
//                                                              //   1 + 0 -
//                                                              //       4 +
//      "modest, lets others have the credit",                  // 1 1 1 0 0
//      "caring, generous, is concerned about others",          // 1 1 1 0 1
//      "",                                                     // 1 1 1 1 0
//      "",                                                     // 1 1 1 1 1
//      "flexible",                                             // 1 1 1 2 0
//      "loyal, obliging, courteous",                           // 1 1 1 2 1
//      "sensitive, soft, tender-hearted",                      // 1 1 1 3 0
//      "patient, positive, easy-going",                        // 1 1 1 3 1
//      "mild-mannered, compliant, accommodating",              // 1 1 1 4 0
//      "fair, reasonable, sincere",                            // 1 1 1 4 1
// 
//                                                              //   2 - 0 -
//                                                              //       4 +
//      "eccentric, absent-minded, inattentive",                // 1 2 0 0 0
//      "wild, foolhardy, devil-may-care",                      // 1 2 0 0 1
//      "negligent, reckless, rash",                            // 1 2 0 1 0
//      "is willing to change his/her plans",                   // 1 2 0 1 1
//      "",                                                     // 1 2 0 2 0
//      "",                                                     // 1 2 0 2 1
//      "fickle, restless, uncontrolled",                       // 1 2 0 3 0
//      "doesnt think of tomorrow",                            // 1 2 0 3 1
//      "indolent, foolish, lax",                               // 1 2 0 4 0
//      "undogmatic, disobedient, reckless",                    // 1 2 0 4 1
// 
//                                                              //   2 + 0 -
//                                                              //       4 +
//      "reliable, cautious, deliberate",                       // 1 2 1 0 0
//      "takes care of things",                                 // 1 2 1 0 1
//      "wants things to proceed according to plan",            // 1 2 1 1 0
//      "neat, industrious, conscientious",                     // 1 2 1 1 1
//      "",                                                     // 1 2 1 2 0
//      "",                                                     // 1 2 1 2 1
//      "loves security",                                       // 1 2 1 3 0
//      "controlled, formal, straightforward",                  // 1 2 1 3 1
//      "law-abiding, obedient, conservative",                  // 1 2 1 4 0
//      "precise, efficient, disciplined",                      // 1 2 1 4 1
// 
//                                                              //   3 - 0 -
//                                                              //       4 +
//      "melancholic, dejected, gloomy",                        // 1 3 0 0 0
//      "wants to be liked",                                    // 1 3 0 0 1
//      "touchy, peevish, ill-tempered",                        // 1 3 0 1 0
//      "hypersensitive, emotional, easily hurt",               // 1 3 0 1 1
//      "unstable, erratic, irrational",                        // 1 3 0 2 0
//      "needlessly worries a lot",                             // 1 3 0 2 1
//      "",                                                     // 1 3 0 3 0
//      "",                                                     // 1 3 0 3 1
//      "panicky, insecure, unbalanced",                        // 1 3 0 4 0
//      "complex, passionate",                                  // 1 3 0 4 1
// 
//                                                              //   3 + 0 -
//                                                              //       4 +
//      "composed, undisturbed, imperturbable",                 // 1 3 1 0 0
//      "optimistic, vigorous, uncomplicated",                  // 1 3 1 0 1
//      "sly, self-satisfied, heartless",                       // 1 3 1 1 0
//      "calm, even-tempered, unworried",                       // 1 3 1 1 1
//      "carefree, happy-go-lucky, laconic",                    // 1 3 1 2 0
//      "stable, well balanced, down-to-earth",                 // 1 3 1 2 1
//      "",                                                     // 1 3 1 3 0
//      "",                                                     // 1 3 1 3 1
//      "corrupt, slippery",                                    // 1 3 1 4 0
//      "confident, fearless, rational",                        // 1 3 1 4 1
// 
//                                                              //   4 - 0 -
//                                                              //       4 +
//      "passive, weak, weak-minded",                           // 1 4 0 0 0
//      "kind-hearted, doting",                                 // 1 4 0 0 1
//      "shallow, shortsighted, hypocritical",                  // 1 4 0 1 0
//      "docile, humble, will agree to anything",               // 1 4 0 1 1
//      "gullible, sluggish, inactive",                         // 1 4 0 2 0
//      "dutiful, ordinary, conventional",                      // 1 4 0 2 1
//      "hesitant, dependent, indecisive",                      // 1 4 0 3 0
//      "unreliable, slimy",                                    // 1 4 0 3 1
//      "",                                                     // 1 4 0 4 0
//      "",                                                     // 1 4 0 4 1
// 
//                                                              //   4 + 0 -
//                                                              //       4 +
//      "analytical, skeptical",                                // 1 4 1 0 0
//      "dauntless, enterprising, inventive",                   // 1 4 1 0 1
//      "knows how to manipulate a situation",                  // 1 4 1 1 0
//      "open-minded, profound, constructive",                  // 1 4 1 1 1
//      "rebellious, undisciplined, revolutionary",             // 1 4 1 2 0
//      "purposeful, determined, decisive",                     // 1 4 1 2 1
//      "thinks deeply about things",                           // 1 4 1 3 0
//      "firm, resolute, clever",                               // 1 4 1 3 1
//      "",                                                     // 1 4 1 4 0
//      "",                                                     // 1 4 1 4 1 
// };

///////////////

char const *Language::s_heShe[][2] =    
    {
        { "zij",  "hij" },
        { "she",  "he" },
    };

char const *Language::s_hisHer[][2] =   
    { 
        {"haar", "zijn"},
        {"her", "his"},
    };

char const *Language::s_himHer[][2] =   
    { 
        {"haar", "hem" },
        {"her",  "him" },
    };

char const *Language::s_areYou[] =   
    { 
        "Bent u",
        "Are you"
    };

char const *Language::s_isIdeal[] =   
    {
        "Is de ideale persoon", 
        "Is the ideal person"
    };

    // NO \n in the following text: not accepted by html
    ////////////////////////////////////////////////////
        
char const *Language::s_close[] =           // std. self/other ratings
    {
        "U heeft alle vragen beantwoord.<p>"
        "Klik op &#x2018;antwoorden opslaan&#x2019; "
                                    "om uw antwoorden op te slaan.",

        "You have answered all questions.<p>"
        "Click &#x2018;save responses&#x2019; " "to save your answers."
    };

char const *Language::s_closeMereSelf[] = 
    {
        "U heeft alle vragen beantwoord.<p>"
        "Klik op &#x2018;maak het rapport&#x2019; om de rapportage "
        "te maken.",

        "You have answered all questions.<p>"
        "Click &#x2018;make the report&#x2019; to make the report."
    };

char const *Language::s_selfThanks[] = 
    {
        "Als de antwoorden van de andere beoordelaars binnen een week zijn "
        "ontvangen dan ontvangt u een e-mail dat de rapportage klaar is. ",

        "If your other-raters have completed the FFPI within one week then "
        "you receive an e-mail informing you that the report is available."
    };

char const *Language::s_otherThanks[] = 
    {
        "Hartelijk dank voor het invullen van de vragenlijst.",

        "Thank you for completing the questionnaire.",
    };

char const *Language::s_mailSubject[][3] =
{
    {   // NL                                       // 2nd index:
        "Verzoek namens ",                          // 0
        " om een vragenlijst in te vullen",         // 1
        "Uw FFPI onderzoeksprojectnummer",          // 2
    },
    {   // EN
        "Request by ",      
        " to fill out a questionnaire",
        "Your FFPI research project number",
    }
};

char const *Language::s_month[][12] =
{
    {   // NL
        "januari",
        "februari",
        "maart",
        "april",
        "mei",
        "juni",
        "juli",
        "augustus",
        "september",
        "oktober",
        "november",
        "december",
    },
    {   // EN
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "Sepember",
        "October",
        "November",
        "December",
    }
};

