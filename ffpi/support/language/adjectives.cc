//#define XERR
#include "language.ih"

string Language::adjectives(Uvector const &indices) const
{
    string ret;

                                // adjectives to use in the report
    for (size_t idx = 0; idx != Enum::N_USE_ADJECTIVES; ++idx)
        ret += s_adjective[indices[idx]].adjective[d_language] + ", ";

    ret.resize(ret.size() - 2);

    return ret;
}
