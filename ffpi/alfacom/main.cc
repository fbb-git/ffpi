#include "main.ih"

void show(Part const &part)
{
    cout << "Line " << part.lineNr << ' ' << part.first << ' ' <<
            part.second << '\n';
}

int main(int argc, char **argv)
try
{
    if (argc == 1)
    {
        cout << "provide the name of the alfacom file\n";
        return 0;
    }

    ifstream in{ Exception::factory<ifstream>(argv[1]) };
    string line;

    size_t lineNr = 0;

    vector<Part> parts;

    // an unordered map of words x indices provides access to 
    while (getline(in, line))           // read all lines
    {
        Part part{ ++lineNr };
        istringstream in(line);         // access the components
        if (not (in >> part.comm >> part.first >> part.second))
            continue;
        
        parts.push_back(move(part));
    }

    stable_sort(parts.begin(), parts.end(), 
        [&](Part const &lhs, Part const &rhs)
        {
            return lhs.second < rhs.second;
        }
    );

    cout << parts.size() << " translations\n" 
            "last is ";
    show(parts.back());
    cout << '\n';

    size_t nReductions = 0;
    for (auto begin = parts.begin(), end = parts.end(); begin != end; )
    {
        auto equal = find_if(begin + 1, end,    // find 1st unequal begin
            [&](Part const &part)
            {
                return part.second != begin->second;
            }
        );

        if (equal - begin == 1)                     // one element, then
            ++begin;                                //  continue at the next
        else                                        // or show all equal elms.
        {
            if (nReductions == 0)
                cout << "Equal translations:\n";

            nReductions += equal - begin - 1;
            for (; begin != equal; ++begin)
                show(*begin);
        }
    }

    cout << '\n' <<
            nReductions << 
                (nReductions == 1 ?
                    " element was"
                :
                    " elements were") << " removed\n";
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cout << "unexpected exception\n";
    return 1;
}


