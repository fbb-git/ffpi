//                     usage.cc

#include "main.ih"

namespace {
char const rslInfo[] = R"_( [options] arg
Where:
    [options] - optional arguments (short options between parentheses):
        --date (-D mm-yyyy) - (01-)mm-yyyy is used as the current date to 
                              check project end-dates. Use mm: 01..12;
        --debug (-d)        - debug state: no changes to the research file
        --log (-L type)     - type of info being logged. With type 'debug' 
                              no changes are made. By default the 
                              etc/ffpi_config log: specification is
                              used. Alternative specs: info, warning, err,
                              crit emerg;
        --help (-h)         - provide this help;
        --list (-l)         - show the research projects info;
        --mail (-m address) - send mail to 'address' instead of to the 
                              configured mail address. Use '--mail no'
                              to suppress sending e-mail;
        --research (-r file)- use the research data file 'file' instead of
                              `std data path'/research. With --research
                              existing 'projectNr'.txt files are not removed
                              but a message is inserted into cout;
        --version (-v)      - show version information and terminate.
    
    arg - specify any arg (e.g., 'go') to perform the task specified using
          the options

)_";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << rslInfo <<
    progname << " must have owner www-data and mode u+s\n"
    "configfile: " << g_options.config() << "\n"
                "\n";
}







