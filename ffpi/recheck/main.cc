//#define XERR
#include "main.ih"

namespace {

    Arg::LongOption longOpts[] =
    {
        Arg::LongOption{"debug",    'd'},
        Arg::LongOption{"date",     'D'},
        Arg::LongOption{"help",     'h'},
        Arg::LongOption{"list",     'l'},
        Arg::LongOption{"log",      'L'},
        Arg::LongOption{"mail",     'm'},
        Arg::LongOption{"research", 'r'},
        Arg::LongOption{"version",  'v'},
    };
    auto longEnd = longOpts + size(longOpts);


#include "../basedir.f" // abs. path to the website's base directory

} // anonymous


Options     g_options{ baseDir };
Language    g_language;
ConfigFile  g_config{ g_options.config(), ConfigFile::RemoveComment };
Mailer      g_mailer;
LogInfo     g_logInfo{ " recheck " };

SyslogStream Syslog::s_sls{ "ffpi recheck" };

int main(int argc, char **argv)
try
{
    Arg const &arg = Arg::initialize("dD:hlL:m:r:v", longOpts, longEnd, 
                                     argc, argv);

    arg.versionHelp(usage, Icmbuild::version, 1);

    g_logInfo.setLevel();

    info() << "(V. " << Icmbuild::version << ") starts" << '\n';

    Projects projects;
    projects.process();
}
catch (Syslog const &syslog)        // Options constructor failed
{
    cerr << "EMERG: Options construction failed, see syslog message\n";
    return 1;
}
catch (int exc)
{
    return Arg::instance().option("hv") ? 0 : exc;
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cout << "unexpected exception\n";
    return 1;
}

