//#define XERR
#include "projects.ih"

void Projects::sendMail(string const &emailAddr) const
{
    string project{ to_string(d_pod.projectNr()) };

    xerr("Calling g_mailer.sendmail sending to " << emailAddr);

    g_language.setLanguage(d_pod.flags());      // use the correct language

                                                // removal date
    Date removal = Tools::normalize(d_pod.year(), d_pod.month() + 1);

    g_mailer.sendmail(
            emailAddr, 
            "Project " + to_string(d_pod.projectNr()),
            DollarText::replaceStream(
                g_options.mailPath() + 
                    g_language.prefix() + "endproject", 
                {
                    project,                            // $0
                    g_language.month(d_pod.month()),    // $1
                    to_string(d_pod.year()),            // $2
                    g_language.month(removal.month),    // $3
                    to_string(removal.year),            // $4
                    g_config.findKeyTail("projecturl:") // $5
                }
            )
    );
}




