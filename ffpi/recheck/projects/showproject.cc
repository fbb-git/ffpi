//#define XERR
#include "projects.ih"

void Projects::showProject() const
{
    cout << "Project " << d_pod.projectNr() << 
        ": flags: " << d_pod.flags() << " (" << 
                                    (d_pod.flags() & 1 ? "EN" : "NL") << ")"
        ", month: " << d_pod.month() <<
        ", year: " << d_pod.year() << "\n"
        "        e-mail: " << d_projectMail << '\n';

    if (d_endDate == d_now)
        cout << "project expired, data will be removed next month\n";
    else if (d_endDate < d_now)
        cout << "ERROR: project should have been removed\n";
}
