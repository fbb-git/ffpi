//#define XERR
#include "projects.ih"

void Projects::putRecord()
{
    d_data.seekp(d_writePos);                   // current write position

    if (
        not Tools::writeN(d_data, &d_pod)       // write the record
        or
        not Tools::writeS(d_data, d_tail)
    )
        throw Exception{} << "cannot write data of project " << 
                             d_pod.projectNr();

    d_writePos = d_data.tellp();
}
