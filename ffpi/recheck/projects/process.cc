//#define XERR
#include "projects.ih"

void Projects::process()
{
    LockGuard lg{ d_research };

    while (getRecord())
    {
        xerr("readPos: " << d_readPos << ", writePos: " << d_writePos);

        switch ((this->*d_task)())                  // list, update
        {
            case NEXT:                              // list, or drop the rec.
            continue;

            case AS_IS:
                if (d_readPos == d_writePos)        // R/W positions before 
                {                                   // getRecord are equal
                    d_writePos += d_size;           // then update writePos
                    continue;                       // and do the next record
                }
            [[fallthrough]];

            case UPDATE:
                putRecord();
                d_data.seekg(d_readPos + d_size);
            continue;
        }
    }

    if (d_dropped)
        resize_file(d_research, d_writePos);
}




