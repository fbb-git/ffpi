#ifndef INCLUDED_PROJECTS_
#define INCLUDED_PROJECTS_

#include <fstream>
#include <string>

#include "../../support/types/types.h"

namespace FBB
{
    class Arg;
}

struct ProjectData;

class Projects
{
    enum Action
    {
        AS_IS,                  // leave the record as-is
        NEXT,                   // list or drop the record
        UPDATE,                 // rewrite the record
    };

    FBB::Arg const &d_arg;
    std::string  d_research;
    std::fstream d_data;

    std::string d_optionMail;
    bool d_debug;

    size_t d_now;               // (year << 8) + month
    size_t d_endDate;           // same, set by getRecord.

    size_t d_readPos = 0;
    size_t d_writePos = 0;
    bool d_dropped = false;     // true: at least one record was dropped

    ProjectData d_pod;          // information retrieved by getRecord
    std::string d_tail;
    std::string d_projectMail;
    size_t d_size;

    Action (Projects::*d_task)();
    void (Projects::*d_mailFun)() const;

    public:
        Projects();
        void process();

    private:
        void showProject() const;

        bool getRecord();
        void putRecord();                   // positions d_data to d_readPos

        Action update();                      // handles '-D, -d, -m
        Action list();

// not used:
        void nop(std::string const &emailAddr);

        void projectMail() const;
        void noMail() const;
        void optionMail() const;    // with -m

        void sendMail(std::string const &emailAddr) const;

        static void changeUser();
};
        
#endif





