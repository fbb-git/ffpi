//#define XERR
#include "projects.ih"

Projects::Action Projects::update()
{
    debug() << "update: end year = " << d_pod.year() << 
                                ", end month: " << d_pod.month() << endl;

    if (d_now < d_endDate)              // no need to handle this project
        return AS_IS;

    if (d_endDate == d_now)             // reached the end-date
    {
        if (d_debug)
        {
            showProject();
            cout << "Project exipred.\n"
                "Mail ";

            if (d_optionMail.empty())
                cout << "sent to " << d_projectMail << '\n';
            else if (d_optionMail == "no")
                cout << "not sent\n";
            else
                cout << "sent to " << d_optionMail << '\n';

            return AS_IS;
        }

        info() << "Project " << d_pod.projectNr() << " expired" << endl;
        (this->*d_mailFun)();
        
        return AS_IS;
    }

    // beyond the current date, so remove the project and the research file

    if (d_debug)
    {
        cout << "Project " << d_pod.projectNr() << 
                                " and its data file would be removed.\n";
        return AS_IS;
    }

    info() << "Removed project " << d_pod.projectNr();

    if (not d_arg.option('r'))
        d_dropped = true;
    else
    {
        error_code ec;
        remove(g_options.researchPath() + 
               to_string(d_pod.projectNr()) + ".txt", ec);
        info() << " and " << d_pod.projectNr() << ".txt";
    }
    
    info() << endl;

    return NEXT;
}


