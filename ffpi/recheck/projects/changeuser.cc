//#define XERR
#include "projects.ih"

//static
void Projects::changeUser()
{
    User current;                                       // current user
    size_t wwwUid = User{ "www-data" }.userid();        // required user

    if (wwwUid != current.eUserid())                    // check effective id
        throw Exception{} << "Effective user id != " << wwwUid << 
                                                                " (www-data)";

    if (setuid(wwwUid) != 0)                            // check changing uid
        throw Exception{} << "Can't change the user id to " << wwwUid << 
                                                                " (www-data)";
}
