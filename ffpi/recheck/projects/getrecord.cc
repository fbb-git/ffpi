//#define XERR
#include "projects.ih"

bool Projects::getRecord()
{
    d_readPos = d_data.tellg();                 // current read position

    if (Tools::readN(d_data, &d_pod) != 1)      // no more records
            return false;

    string iv;
    Tools::readS(d_data, iv, Enum::IV_SIZE);    // read the IV

    string enc;
    Tools::readS(d_data, enc, d_pod.encryptSize()); // read the encrypted
                                                    // e-mail address

                                                // total size of the record 
    d_size = static_cast<size_t>(d_data.tellg()) - d_readPos;

    d_tail = iv + enc;                          // record info beyond the
                                                // ProjectData
    
    d_projectMail = Tools::decrypt(iv, enc);    // get the project's mail
                                                // addr.

    xerr(__FILE__ ": yr = " << d_pod.year() << ", mon = " << d_pod.month());

    d_endDate = Tools::asNumber(d_pod.year(), d_pod.month());
    
    return true;
}






