// #define XERR
#include "projects.ih"

Projects::Projects()
:
    d_arg(Arg::instance()),
    d_debug(d_arg.option('d'))
{
    changeUser();

    if (not d_arg.option(&d_research, 'r'))
        d_research = g_options.dataPath() + "research";
    
    d_data = Exception::factory<fstream>(d_research, 
                                        ios::in | ios::out, 
                                        ios::in | ios::out | ios::trunc);
    d_task = 
        d_arg.option('l') ? &Projects::list    : // list the projects to cout
                            &Projects::update;   // update 

    d_mailFun = not d_arg.option(&d_optionMail, 'm') ? 
                                                &Projects::projectMail :
                d_optionMail == "no" ?          &Projects::noMail      :
                                                &Projects::optionMail;

//  logType();d_arg.option('d');

    string value;                               // mm-yyyy is expected
    d_now = 
        d_arg.option(&value, 'D') ?
            Tools::asNumber(stoul(value.substr(3)), stoul(value) - 1)
        :
            Tools::asNumber(DateTime{ DateTime::LOCALTIME });
}


