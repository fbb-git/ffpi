To install the ffpi files run

    install [option] websitebase

E.g.,

    install -a active

Where [option] is an optional argument which, if provided, must be -a or
--all. When the option is specified the script ./build is called to
(re)compile all FFPI programs.

The argument `websitebase' is the website's base directory, which is created
if non-existing.  Below this base directory the following file and directories
are defined. Note the group permission +w for the entries shown with a *(the
username may be different, i.e., the username of the user installing the
program. E.g., at https://ffpi.nl it is 'ffpi':

drwxr-xr-x  2 frank www-data    bin/
drwxr-xr-x  2 frank www-data    config/
drwxr-xr-x  2 frank www-data    css/
drwxrwxr-x  2 frank www-data    data/           *
drwxr-xr-x  2 frank www-data    etc/
drwxr-xr-x  3 frank www-data    html/
drwxr-xr-x  2 frank www-data    images/
drwxr-xr-x  2 frank www-data    js/
-rw-rw-r--  1 frank www-data    log             *
drwxr-xr-x  3 frank www-data    mail/
drwxr-xr-x  3 frank www-data    molds/
drwxr-xr-x  2 frank www-data    questions/
drwxrwxr-x  2 frank www-data    reports/        *
drwxrwxr-x  2 frank www-data    research/       *
drwxrwxr-x  2 frank www-data    tmp/            *
drwxr-xr-x  3 frank www-data    www/

In the etc/ directory the file fpi_config specifies the options that can be
selected before starting the 'form' program.

The 'install' script ensures that all these directories are available having
the correct group permissions.

The website's base path must also be specified in apache's configuration, see
website.skel/config/apache/config for an example.

When running 'install -a ...' the 'crypttab' program defines the parameters
that are used for encrypting the personal data in the files in the ./data
directory. It writes the file 'endecrypt1.cpp' defining the class EnDeCrypt's
constructor, compiles it, then removes 'endecrypt1.cpp' and installs
'endecrypt1.o' in libsupport.a.

(at ballistix it is /home/frank/src/3ppq/website/www, defined as 'basePath')

Run this script as the destination user from the location where this script
is located. 

The destination user should be a member of the group www-data.
The target directory may be specified as a relative or absolute path.

