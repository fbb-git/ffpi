#include <iostream>
#include <fstream>

#include <bobcat/exception>

#include "../support/types/enum.h"

using namespace std;
using namespace FBB;

int main(int argc, char **argv)
try
{
    auto file{ Exception::factory<ofstream>(argv[1], ios::in | ios::out) };

    file.seekp(Enum::N_QUESTIONS);
    file.put(0);
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
