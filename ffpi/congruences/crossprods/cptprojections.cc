#include "crossprods.ih"

    // lhs: N x 5, rhs: k x 5, the cols are the common dimensions
    // the resulting matrix has dimension N x k
void CrossProds::cptProjections()
{
    VVD const &lhs = d_data.scores();   // scores of N subjects on f factors
    VVD const &rhs = d_data.loadings(); // loadings of k adjectives on f fact.

    if (d_verbose)
        cout << "cross-products\n";

    d_projections.clear();

    for (size_t row = 0; row != lhs.size(); ++row)  // visit all f-scores rows
    {
        VD ip;
        for (size_t col = 0; col != rhs.size(); ++col)
        {
            double value = inner_product(lhs[row].begin(), lhs[row].end(), 
                              rhs[col].begin(), 0.0);

            ip.push_back(value);
        }
        d_projections.push_back(ip);
    }
}

