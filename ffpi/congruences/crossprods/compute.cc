//#define XERR
#include "crossprods.ih"

void CrossProds::compute()
{
    cptProjections();
    cptCongruences();
}
