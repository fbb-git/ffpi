#ifndef INCLUDED_CROSSPRODS_
#define INCLUDED_CROSSPRODS_

#include "../types/types.h"
class Data;

class CrossProds
{
    bool d_verbose;
    Data const &d_data;
    VU d_indices;
    VVD d_projections;
    VVD d_congruences;

    public:
        CrossProds(Data const &data);

        void compute();                     // projections: scores * loadings
                                            // congruences: proj / sqrt(
                                            //  scoresSS * loadingsSS)

        
        VVD const &projections() const;
        VVD const &congruences() const;
        
    private:
        void cptProjections();
        void cptCongruences();
};

inline VVD const &CrossProds::projections() const
{
    return d_projections;
}
        
inline VVD const &CrossProds::congruences() const
{
    return d_congruences;
}
        
#endif



