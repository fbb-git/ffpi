#include "crossprods.ih"

void CrossProds::cptCongruences()
{
    d_congruences = d_projections;

    VD const &scoresSS = d_data.scoresSS();
    VD const &loadingsSS = d_data.loadingsSS();

    if (d_verbose)
        cout << "congruences\n";

    for (size_t pp = 0; pp != scoresSS.size(); ++pp)    // visit all subjects
    {
        double rowSS = scoresSS[pp];                    // SS of subject pp

                    // visit all adjectives
                    // compute congruence of pp & adj as
                    // F * A' / sqrt( SS[pp] * loadingsSS[adj] )

        for (size_t adj = 0; adj != loadingsSS.size(); ++adj)
            d_congruences[pp][adj] /= sqrt(rowSS * loadingsSS[adj]);
    }
}

        
