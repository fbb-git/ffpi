//#define XERR
#include "crossprods.ih"

CrossProds::CrossProds(Data const &data)
:
    d_verbose(Arg::instance().option('V')),
    d_data(data)
{}
