//#define XERR
#include "display.ih"

Display::Display(size_t nAdj, Data const &data, CrossProds const &crossProds)
:
    d_nAdj(nAdj),
    d_data(data),
    d_crossProds(crossProds),
    d_indices(makeIndices(data.nAdjectives()))
{
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(5);
}
