//#define XERR
#include "display.ih"

void Display::fillAdjCongruence(Table &table, size_t idx)
{
    ostringstream out;
    out.setf(ios::fixed, ios::floatfield);
    out.precision(2);

    auto pair = randomPair();

    out << (
                inner_product(
                        d_data.loadings()[pair.first].begin(),
                        d_data.loadings()[pair.first].end(),
                        d_data.loadings()[pair.second].begin(),
                        0.0)
                / sqrt(d_data.loadingsSS()[pair.first] * 
                   d_data.loadingsSS()[pair.second])
            );
            

    table << to_string(idx + 1) << d_data.adjectives()[pair.first] <<
                                d_data.adjectives()[pair.second] << 
                                out.str();
}
