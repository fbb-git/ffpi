//#define XERR
#include "display.ih"

size_t Display::removeRandomValue()
{
    size_t index = random() % d_indices.size();
    size_t ret = d_indices[index];

    d_indices.erase(d_indices.begin() + index);
    return ret;
}
