//#define XERR
#include "display.ih"

size_t Display::nPositiveLoadingsOn(size_t factor, size_t nPairs)
{
    d_indices.clear();

    for (size_t idx = 0, end = d_data.adjectives().size(); idx != end; ++idx)
    {
        if (d_data.loadings()[idx][factor] > 0)
            d_indices.push_back(idx);
    }

    if (size_t nPos = d_indices.size(); nPos < 2 * nPairs)
    {
        cout << "Only " << nPos << 
                " adjectives have positive loadings on factor 1\n"
                "Selecting "  << (nPos / 2) << " instead of " << nPairs <<
                " pairs\n";
        nPairs = nPos / 2;
    }

    return nPairs;
}
