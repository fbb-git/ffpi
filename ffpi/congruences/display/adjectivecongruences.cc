//#define XERR
#include "display.ih"

void Display::adjectiveCongruences(size_t nPairs)
{
    srandom(time(0));
                                        // d_indices: indices of positive 
                                        // adjectives
                                        // not enough positive loading adj?
    nPairs = nPositiveLoadingsOn(0, nPairs); 

    if (nPairs == 0)
        return;

    TableLines tableLines;
    tableLines << "  " << "  " << "  " << "  ";

    tableLines << TableSupport::HLine{1, 0, 4};

    Table table{ tableLines, 4, Table::ROWWISE };
    
    table << "nr." << "adj." << "adj." << "congr.";

                                        // randomly selected pairs from 
                                        // d_indices
    for (size_t idx = 0; idx != nPairs; ++idx)
        fillAdjCongruence(table, idx);

    cout << table << '\n';
}

