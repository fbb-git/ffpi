#ifndef INCLUDED_DISPLAY_
#define INCLUDED_DISPLAY_

#include <iosfwd>

#include "../types/types.h"

class Data;
class CrossProds;

namespace FBB
{
    class Table;
}

class Display
{
    size_t d_nAdj;
    Data const &d_data;
    CrossProds const &d_crossProds;

    VVU d_subjIndices;
    VU d_indices;

    public:
        Display(size_t nAdj, Data const &data, CrossProds const &crossProds);
        void projections();
        void congruences();
        void projectionsCongruences() const;    // show sums of proj. + cong,
                                                // with congr >= .866

        void congruencesProjections() const;    // show max. cong, +
                                                // matching projection

        void congrProjSums() const;             // show sums of cong +
                                                // matching projection

        void congruencesFscores() const;       // show congruences between
                                                // pairs of F-scores

                                                // show congruences of nPairs 
                                                // of adjectives having
                                                // positive loadings on factor
                                                // 0
        void adjectiveCongruences(size_t nPairs);

    private:
        void showPairs(size_t row) const;                               // 1
        void showPairs(size_t row, VU const &indices, size_t count) const;// 2

        void showMaxCong(size_t row) const;
        void showMaxCongProj(size_t row) const;

        void show(VVD const &data, char const *label);
        void show(size_t rowNr, VD const &data);
        void show(VD const &data, char const *label);

        size_t sortSums(VU &indices, size_t row) const;  // sort 
                                            // sums of congr. and proj.

        size_t nPositiveLoadingsOn(size_t factor, size_t nPairs);
        std::pair<size_t, size_t> randomPair();
        size_t removeRandomValue();         // remove from d_indices
        void fillAdjCongruence(FBB::Table &table, size_t idx);

        static VU makeIndices(size_t end);
};
        
#endif


