#include "display.ih"

void Display::showPairs(size_t row, VU const &indices, size_t count) const
{
    cout << "Congruence and projections:\n";

    auto const &projections = d_crossProds.projections()[row];
    auto const &congruences = d_crossProds.congruences()[row];
    auto const &adjectives = d_data.adjectives();

    if (count > d_nAdj)
        count = d_nAdj;

    for (size_t idx = 0; idx != count; ++idx)
    {
        size_t index = indices[idx];

        cout << setw(15) << adjectives[index] << 
                ": " << setw(8) << congruences[index] << 
                ", " << setw(8) << projections[index] << 
                " (" << setw(8) << 
                        (congruences[index] + projections[index]) << ")"
                ".      ";

//        for (size_t factor = 0; factor != 5; ++factor)
//            cout <<  "  " << loadings[index][factor];

        cout  << '\n';
    }
}






