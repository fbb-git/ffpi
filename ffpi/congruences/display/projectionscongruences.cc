//#define XERR
#include "display.ih"

void Display::projectionsCongruences() const  // show sums of proj. + cong, 
{                                       // with cong >= .86 (= < 30 degr.)
    cout << "\n"
            "Sums of congruences (> 0.866) and projections:\n";

    for (
        size_t row = 0, end = d_crossProds.projections().size(); 
            row != end; 
                ++row
    )
        showPairs(row);
}
