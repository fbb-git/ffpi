//#define XERR
#include "display.ih"

void Display::congrProjSums() const
{
    cout << "\n"
            "Max. sums of congruences and corresponding projections:\n"
            "\n"
            "      adjective   sum      congruence projection\n";
    
    for (
        size_t pp = 0, end = d_crossProds.congruences().size(); 
            pp != end; 
                ++pp
    )
        showMaxCongProj(pp);
}
