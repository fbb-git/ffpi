#include "display.ih"

// static
VU Display::makeIndices(size_t end)
{
    VU ret;

    for (size_t idx = 0; idx != end; ++idx)
        ret.push_back(idx);

    return ret;
}
