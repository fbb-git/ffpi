#include "display.ih"

void Display::showMaxCongProj(size_t pp) const
{
    cout << "\nSubject row " << (pp + 1) << ":\n";

    VU indices{ d_indices };
    VS const &adjectives = d_data.adjectives();

        // sort the congruences
    auto const &congruences = d_crossProds.congruences()[pp];

    double sqrtSSscores = sqrt(d_data.scoresSS()[pp]);

    VD projections;
    for (auto cong: congruences)            // compute projection of SS
        projections.push_back(cong *        // scores on the adjectives
                              sqrtSSscores);
    

    sort(indices.begin(), indices.end(),
        [&](size_t lhs, size_t rhs)
        {
            return congruences[lhs] + projections[lhs] > 
                    congruences[rhs] + projections[rhs];
        }
    );

        // show the d_nAdj highest sums of congruences and the matching
        // projection 
    for (size_t idx = 0; idx != d_nAdj; ++idx)
    {
        size_t index = indices[idx];
        double congruence = congruences[index];
        double projection = projections[index];

        cout << setw(15) << adjectives[index] << 
                ": " << setw(8) << congruence + projection << 
                " (" << setw(8) << congruence << ", " <<
                                    setw(8) << projection <<
                ")\n";
    }

    string endCount;
    if (Arg::instance().option(&endCount, 'e') == 0)
        return;

    cout << "-----\n";
    
    for (
        size_t idx = indices.size(), end = idx - stoul(endCount);
            idx-- != end;
    )
    {
        size_t index = indices[idx];
        double congruence = congruences[index];
        double projection = projections[index];

        cout << setw(15) << adjectives[index] << 
                ": " << setw(8) << congruence + projection << 
                " (" << setw(8) << congruence << ", " <<
                                    setw(8) << projection <<
                ")\n";
    }


}





