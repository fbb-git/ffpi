#include "display.ih"

void Display::show(VD const &data, char const *label)
{
    cout << label << " values:\n";

    VS const &adjectives = d_data.adjectives();
    VVD const &loadings = d_data.loadings();

    for (size_t idx = 0; idx != d_nAdj; ++idx)
    {
        size_t index = d_indices[idx];

        cout << "   " << setw(8) << 
                data[index] << ": " << setw(15) << adjectives[index] <<
                ".      ";

        cout.precision(3);

        for (size_t factor = 0, end = loadings[0].size(); 
                factor != end; ++factor
        )
            cout <<  ' ' << setw(6) << loadings[index][factor];

        cout.precision(5);

        cout  << '\n';
    }
}
