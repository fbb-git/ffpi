#include "display.ih"

void Display::showMaxCong(size_t pp) const
{
    cout << "\nSubject row " << (pp + 1) << ":\n";

    VU indices{ d_indices };
    VS const &adjectives = d_data.adjectives();

        // sort the congruences
    auto const &congruences = d_crossProds.congruences()[pp];
    sort(indices.begin(), indices.end(),
        [&](size_t lhs, size_t rhs)
        {
            return congruences[lhs] > congruences[rhs];
        }
    );

    double sqrtSSscores = sqrt(d_data.scoresSS()[pp]);

        // show the d_nAdj highest congruences and the matching
        // projection by multiplying the congr. by sqrt(adjSS[index])
    for (size_t idx = 0; idx != d_nAdj; ++idx)
    {
        size_t index = indices[idx];
        double congruence = congruences[index];

        cout << setw(15) << adjectives[index] << 
                ": " << setw(8) << congruence << 
                " (" << setw(8) << congruence * sqrtSSscores <<
                ")\n";
    }

    string endCount;
    if (Arg::instance().option(&endCount, 'e') == 0)
        return;

    cout << "-----\n";
    
    for (
        size_t idx = indices.size(), end = idx - stoul(endCount);
            idx-- != end;
    )
    {
        size_t index = indices[idx];
        double congruence = congruences[index];

        cout << setw(15) << adjectives[index] << 
                ": " << setw(8) << congruence << 
                " (" << setw(8) << congruence * sqrtSSscores <<
                ")\n";
    }


}





