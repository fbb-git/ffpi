#include "display.ih"

void Display::show(VVD const &data, char const *label)
{
    cout << '\n' << label << ":\n";

    d_subjIndices.clear();

    for (size_t row = 0; row != data.size(); ++row)
        show(row + 1, data[row]);
}
