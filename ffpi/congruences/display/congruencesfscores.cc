//#define XERR
#include "display.ih"

void Display::congruencesFscores() const
{
    //size_t nFactors = d_data.scores()[0].size();

    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);

    cout << "pair  (   rows )  congruence\n"
            "----------------------------\n";

    for (
        size_t pair = 1, pp = 0, end = d_data.scoresSS().size(); 
            pp != end; 
                pp += 2, ++pair
    )
        cout << setw(3) << pair << "   (" << 
                setw(3) << (pp + 1) << ", " << 
                setw(3) << (pp + 2) << "):     " <<
            (
                inner_product(
                            d_data.scores()[pp].begin(),
                            d_data.scores()[pp].end(),
                            d_data.scores()[pp + 1].begin(),
                            0.0)
                / sqrt(d_data.scoresSS()[pp] * d_data.scoresSS()[pp + 1])
            ) << '\n';

    cout << "----------------------------\n";
}

