#include "display.ih"

void Display::show(size_t rowNr, VD const &data)
{
    sort(d_indices.begin(), d_indices.end(),
        [&](size_t lhs, size_t rhs)
        {
            return data[lhs] > data[rhs];
        }
    );

    d_subjIndices.push_back(d_indices);

    cout << "\nSubject row " << rowNr << ":\n";

    show(data, "highest");
}


