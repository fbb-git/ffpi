#include "display.ih"

void Display::showPairs(size_t row) const
{
    cout << "\nSubject row " << (row + 1) << ":\n";

    VU indices{ d_indices };
    size_t end = sortSums(indices, row);

    showPairs(row, indices, end);
}


