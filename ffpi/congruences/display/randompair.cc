//#define XERR
#include "display.ih"

pair<size_t, size_t> Display::randomPair()
{
    return pair<size_t, size_t>{ removeRandomValue(), removeRandomValue() };
}
