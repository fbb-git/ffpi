//#define XERR
#include "display.ih"

void Display::congruencesProjections() const
{
    cout << "\n"
            "Max. congruences and corresponding projections:\n";
    
    for (
        size_t pp = 0, end = d_crossProds.congruences().size(); 
            pp != end; 
                ++pp
    )
        showMaxCong(pp);
}
