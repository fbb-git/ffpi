//#define XERR
#include "display.ih"

size_t Display::sortSums(VU &indices, size_t row) const
{
    auto const &projections = d_crossProds.projections()[row];
    auto const &congruences = d_crossProds.congruences()[row];

    sort(indices.begin(), indices.end(),
        [&](size_t lhs, size_t rhs)
        {
            return congruences[lhs] + projections[lhs] 
                    > 
                   congruences[rhs] + projections[rhs];
        }
    );
 
    return remove_if(indices.begin(), indices.end(), 
                 [&](size_t idx)
                 {
                     return congruences[idx] < 0.866;
                 }
             ) - indices.begin();
}
