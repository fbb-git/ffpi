//                     usage.cc

#include "main.ih"

namespace {
char const info[] = R"_( [options] k scores
Where:
    [options] - optional arguments (short options between parentheses):
        --adjectives (-a) adj   - adj: text file containing K adjectives
                                   (by default: adjectives.txt)
        --display (-d) type     - display indicator:
                                    a: projections
                                    b: congruences
                                    c: sums of projections and congruences
                                    d: max congruence and matching projection
                                    e: sums of congruence and matching 
                                        projections
                                    f: congruences between subsequent pairs
                                        of factor scores (argument 'k' not
                                        used, but must be specified)
                                    g: show congruences of max 100 pairs of 
                                       adjectives having positive loadings on
                                       factor 0
                                  combinations are also possible
        --end (-e) nr       - in addition to the k highest ratings also show
                              the <nr> lowest ratings (only for 
                              display = 1000)
        --help (-h)         - provide this help
        --loadings (-l) loadings - loadings: text file containing K x F factor
                                   loadings (by default: loadings.txt)
        --normalize (-n)    - normalize the F-scores (vs: use as-is)
        --skip (-s)         - skip the first column of the subject factor
                              scores (1st factor score is at column 2)
        --verbose (-V)      - provide info about performed actions
        --version (-v)      - show version information and terminate
    
    k - show the highest k congruences for each subject
    scores - matrix of N x ([-s] + F) subject factor scores

)_";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;
}
