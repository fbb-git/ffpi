#ifndef INCLUDED_TYPES_H_
#define INCLUDED_TYPES_H_

#include <string>
#include <vector>

typedef std::vector<std::string>    VS;

typedef std::vector<unsigned>       VU;
typedef std::vector<VU>             VVU;

typedef std::vector<int>            VI;

typedef std::vector<double>         VD;
typedef std::vector<VD>             VVD;

#endif
