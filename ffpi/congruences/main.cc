#define XERR
#include "main.ih"

namespace
{
    Arg::LongOption longOpts[] =
    {
        Arg::LongOption{"adjectives", 'a'},
        Arg::LongOption{"display", 'd'},
        Arg::LongOption{"end", 'e'},
        Arg::LongOption{"help", 'h'},
        Arg::LongOption{"loadings", 'l'},
        Arg::LongOption{"skip", 's'},
        Arg::LongOption{"verbose", 'V'},
        Arg::LongOption{"version", 'v'},
    };
    auto longEnd = longOpts + size(longOpts);
}


int main(int argc, char **argv)
try
{
    Arg const &arg = Arg::initialize("a:d:e:hl:nsvV", longOpts, longEnd, 
                                     argc, argv);
    arg.versionHelp(usage, Icmbuild::version, 2);


    Data data;

    data.read(arg.option('n') ?
                    Data::NORMALIZE
                :
                    Data::SS);          // normalize the F-scores 
                                        // (WKB 4 Nov 2019 20:38)
                                        // vs. use their SSs

    
    CrossProds crossProds(data);
    crossProds.compute();

                                        // arg1: the number of adjectives to
                                        //       show 
    Display display(stoul(arg[0]), data, crossProds);      

    string displayOpt;
    arg.option(&displayOpt, 'd');

    if (displayOpt.find('a') != string::npos)
        display.projections();              // show the projections

    if (displayOpt.find('b') != string::npos)
        display.congruences();              // show the congruences

    if (displayOpt.find('c') != string::npos)
        display.projectionsCongruences();   // show sums of projections and
                                            // congr. 

    if (displayOpt.find('d') != string::npos)
        display.congruencesProjections();   // show max congr. + corresponding
                                            // projections

    if (displayOpt.find('e') != string::npos)
        display.congrProjSums();            // show sums of congr. +
                                            // corresponding projections

    if (displayOpt.find('f') != string::npos)
        display.congruencesFscores();       // show congruences betweem pairs
                                            // of factorscores

    if (displayOpt.find('g') != string::npos)
        display.adjectiveCongruences(100);  // show congruences between 100
                                            // pairs of adjectives loading
                                            // positively on factor 1



//    auto projIndices{ subjIndices };    // save the indices pointing at the 
//                                        // max. projections
//
//    subjIndices.clear();                // prepare for the next show/sort
//

//    scatter(10, projIndices, projections, congr, 
//                "projections", "congruences");
//
//    scatter(10, subjIndices, congr, projections, 
//                "congruences", "projections" );
//
//    showPairs();
//
//    communalities();

}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
catch (...)
{
    return 1;
}



