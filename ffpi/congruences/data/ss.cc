#include "data.ih"

// static
VD Data::ss(VVD const &matrix) 
{
    VD ret;

    for (size_t idx = 0; idx != matrix.size(); ++idx)
        ret.push_back(
            inner_product(matrix[idx].begin(), matrix[idx].end(), 
                          matrix[idx].begin(), 0.0)
        );

    return ret;
}
