#include "data.ih"

VD Data::normalizeScores() 
{
    for (size_t idx = 0; idx != d_scores.size(); ++idx)
    {
        double ss = sqrt(inner_product(d_scores[idx].begin(), 
                                       d_scores[idx].end(), 
                                       d_scores[idx].begin(), 0.0)
                        );

        for_each(d_scores[idx].begin(), d_scores[idx].end(), 
            [&](double &value)
            {
                value /= ss;
            }
        );
    }

    return VD( d_scores.size(), 1.0 );        // normalized, so SS == 1
}
