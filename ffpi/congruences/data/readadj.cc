#include "data.ih"

// static
VS Data::readAdj(string const &fname)
{
    ifstream in = Exception::factory<ifstream>(fname);

    VS ret;

    copy(istream_iterator<string>(in), istream_iterator<string>(),
        back_inserter(ret));

    return ret;
}
