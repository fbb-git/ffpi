//#define XERR
#include "data.ih"

void Data::read(Type type)
{
    d_scores = read(d_arg[1], 
                d_arg.option('s') == 1);// read the scores, skipping the 1st
                                        // (linenr indicator) element
    d_scoresSS = (type == NORMALIZE) ?
                    normalizeScores()
                :
                    ss(d_scores);

    string display;
    d_arg.option(&display, 'd');

        // read loadings and adjectives for display options a..e
    if ("abcdeg"s.find_first_of(display) != string::npos)
    {
        string value = "loadings.txt";
        d_arg.option(&value, 'l');
    
        d_loadings = read(value);
        d_loadingsSS = ss(d_loadings);      // compute the communalities
                                            // (SS of the loadings)
    
        value = "adjectives.txt";           // read adjectives
        d_arg.option(&value, 'a');
        d_adjectives = readAdj(value);
    }
                                        // show 1st and last adjectives
    if (d_arg.option('V'))
        cout << d_adjectives.size() << " adjectives: " << 
            d_adjectives.front() << " ... " << d_adjectives.back() << '\n';    
}
