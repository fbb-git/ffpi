#ifndef INCLUDED_DATA_
#define INCLUDED_DATA_

#include "../types/types.h"

namespace FBB
{
    class Arg;
}

class Data
{
    FBB::Arg const &d_arg;
    VVD d_loadings;             // the loadings of K adjectives on F factors
    VD  d_loadingsSS;           // Sum of squares of the loadings
                                // (communalities) 

    VVD d_scores;               // the scores of N subject on F factors
    VD  d_scoresSS;             // Sum of squares of the scores

    VS  d_adjectives;           // K adjectives

    public:
        enum Type
        {
            SS,
            NORMALIZE
        };

        Data();

        void read(Type type);   // normalize the F-scores or not.

        size_t nAdjectives() const;
        VVD const &scores() const;
        VVD const &loadings() const;

        VD const &scoresSS() const;
        VD const &loadingsSS() const;

        VS const &adjectives() const;

    private:
        static VD ss(VVD const &matrix);

        VD normalizeScores();   // normalize the F-scores 
                                // (WKB 4 Nov 2019 20:38)

        static VVD read(std::string const &fname, size_t skip = 0);
        static VS readAdj(std::string const &fname);
};

inline size_t Data::nAdjectives() const
{
    return d_adjectives.size();
}

inline VVD const &Data::scores() const
{
    return d_scores;
}
        
inline VVD const &Data::loadings() const
{
    return d_loadings;
}
        
inline VD const &Data::scoresSS() const
{
    return d_scoresSS;
}
        
inline VD const &Data::loadingsSS() const
{
    return d_loadingsSS;
}
        
inline VS const &Data::adjectives() const
{
    return d_adjectives;
}
        
#endif


