#include "data.ih"

        // static
VVD Data::read(string const &fname, size_t skip)
{
    VVD ret;

    ifstream in = Exception::factory<ifstream>(fname);

    string line;
    size_t lineNr = 0;
    while (getline(in, line))
    {
        ++lineNr;

        istringstream istr{line};

        string item;
        for (size_t idx = 0; idx != skip; ++idx)
            istr >> item;
    
        VD row(5);
        for (size_t idx = 0; idx != row.size(); ++idx)
            istr >> row[idx];
            
        if (not istr)
        {
            istr.clear();
            char ch = istr.peek();
            throw Exception{} << "Line nr. " << lineNr << ", col[" <<
                        istr.tellg() << "]: char. '" << ch << "' unexpected";
        }

        ret.push_back(row);
    }

    if (Arg::instance().option('V'))
        cout << fname << ": nrows = " << ret.size() << '\n';

    return ret;
}

    
        
