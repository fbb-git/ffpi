set terminal eps color  size 33 cm, 22 cm

# /home/frank/git/ffpi/src/ffpi/active/tmp/6007.eps: path of the .eps file to create. 
set output "self.eps"

load 'settings.gp'


#     ''   index 2 with linespoints ls 3 title 'NL' at 0.79,0.91

# /home/frank/git/ffpi/src/ffpi/active/tmp/6007.dat: path of the data file. 
plot 'gpself.dat' \
          index 0 with linespoints ls 2 title 'Zelf' at 0.26,0.91,\
     ''   index 1 with linespoints ls 3 title 'NL' at 0.79,0.91

