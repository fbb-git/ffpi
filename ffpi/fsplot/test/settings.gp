
unset border
unset xtics
unset ytics

#   lt  -   linetype
#   lc  -   linecolor
#   lw  -   linewidth
#   pt  -   pointtype
#   ps  -   pointsize


#           L   R   B  T
set margins 15, 15, 7, 7

set xrange [0:6]
set yrange [-3.0:3.5]

#   Legenda Font 
set key font ",22"

# Factor labels, U and D    org: 3.4, -2.9
set label "Extravert"       at 1, 3.0 center font ",22" 
set label "Introvert"       at 1,-2.5 center font ",22" 

set label "Mild"            at 2, 3.0 center font ",22" 
set label "Hard"            at 2,-2.5 center font ",22" 

set label "Ordelijk"        at 3, 3.0 center font ",22" 
set label "Slordig"         at 3,-2.5 center font ",22" 

set label "Emot. Stabiel"   at 4, 3.0 center font ",22" 
set label "Instabiel"       at 4,-2.5 center font ",22" 

set label "Autonoom"        at 5, 3.0 center font ",22" 
set label "Afhankelijk"     at 5,-2.5 center font ",22" 

# Vertical axes, L and R
set arrow from 0,-3   to 0,3.5 lw 2 nohead
set arrow from 6,-3   to 6,3.5 lw 2 nohead

# 0-value line
set arrow from 0,0 to 6,0   lw 2 nohead

# Horizontal axis, U and D
set arrow from 0,3.5 to 6,3.5 lw 2 nohead
set arrow from 0,-3  to 6,-3  lw 2 nohead

# Numbers alongside L, R axes
set label '> 3.5'  at  -.05,3.5  right font ",20" 
set label '3.0'    at  -.05,3.0  right font ",20" 
set label '2.5'    at  -.05,2.5  right font ",20" 
set label '2.0'    at  -.05,2.0  right font ",20" 
set label '1.5'    at  -.05,1.5  right font ",20" 
set label '1.0'    at  -.05,1.0  right font ",20" 
set label '0.5'    at  -.05,0.5  right font ",20" 
set label '0'      at  -.05,0    right font ",20" 
set label '-0.5'   at  -.05,-0.5 right font ",20" 
set label '-1.0'   at  -.05,-1   right font ",20" 
set label '-1.5'   at  -.05,-1.5 right font ",20" 
set label '-2.0'   at  -.05,-2   right font ",20" 
set label '-2.5'   at  -.05,-2.5 right font ",20" 
set label '< -3.0' at  -.06,-3   right font ",20" 

#   Tic marks in L, R vertical axes
#   L-axis 
set arrow from  -.025,3    to .025,3     lw 2 nohead
set arrow from  -.025,2.5  to .025,2.5   lw 2 nohead
set arrow from  -.025,2    to .025,2     lw 2 nohead
set arrow from  -.025,1.5  to .025,1.5   lw 2 nohead
set arrow from  -.025,1    to .025,1     lw 2 nohead
set arrow from  -.025,0.5  to .025,0.5   lw 2 nohead
set arrow from  -.025,-0.5 to .025,-0.5  lw 2 nohead
set arrow from  -.025,-1   to .025,-1    lw 2 nohead
set arrow from  -.025,-1.5 to .025,-1.5  lw 2 nohead
set arrow from  -.025,-2   to .025,-2    lw 2 nohead
set arrow from  -.025,-2.5 to .025,-2.5  lw 2 nohead

#   R-axis 
set arrow from  5.975,3    to 6.025,3     lw 2 nohead
set arrow from  5.975,2.5  to 6.025,2.5   lw 2 nohead
set arrow from  5.975,2    to 6.025,2     lw 2 nohead
set arrow from  5.975,1.5  to 6.025,1.5   lw 2 nohead
set arrow from  5.975,1    to 6.025,1     lw 2 nohead
set arrow from  5.975,0.5  to 6.025,0.5   lw 2 nohead
set arrow from  5.975,-0.5 to 6.025,-0.5  lw 2 nohead
set arrow from  5.975,-1   to 6.025,-1    lw 2 nohead
set arrow from  5.975,-1.5 to 6.025,-1.5  lw 2 nohead
set arrow from  5.975,-2   to 6.025,-2    lw 2 nohead
set arrow from  5.975,-2.5 to 6.025,-2.5  lw 2 nohead

set style line 1 dt 1   lc rgb '#000000'    lw 6 pt 5 ps 1
set style line 2 dt 2   lc rgb '#000000'    lw 8 pt 2 ps 1
set style line 3 dt 3   lc rgb '#000000'    lw 8 pt 2 ps 1
