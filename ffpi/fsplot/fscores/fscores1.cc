#include "fscores.ih"

Fscores::Fscores(char const *indicatorsFname,
                 Dvector2 const &data, Enum::RatingType type)
:
    d_data(data),
    d_fscores(data.size(), vector<double>(Enum::N_FACTORS) ),
    d_type(type)
{
    compute(indicatorsFname);       // compute the factor scores:
                                    //  1 row means: ideal
                                    //  2 rows: self + avg.other,

    d_fscores.push_back(s_NLavg);   // append the NL average values
}

