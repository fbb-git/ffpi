#ifndef INCLUDED_FSCORES_
#define INCLUDED_FSCORES_

#include <iosfwd>

#include "../../support/types/enum.h"
#include "../../support/types/types.h"
#include "../../support/tools/tools.h"

// d_fscores contains the factor scores. For self/ideal 1 row of 5 doubles,
//  for standard (self + others) 2 rows.

class Fscores
{
    Dvector2 d_data;            // incoming raw scores

    Dvector2 d_fscores;
    Enum::RatingType d_type;      // SELF or IDEAL

    static double s_weights[Enum::N_FACTORS][Enum::N_QUESTIONS];
    static double s_selfSDinv[Enum::N_QUESTIONS];
    static double s_otherSDinv[Enum::N_QUESTIONS];
    static Dvector s_NLavg;

    public:
        Fscores(char const *indicatorsFname, 
                Dvector2 const &data, Enum::RatingType type);

        Dvector2 const &scores() const;     // Factor scores. The last row 
                                            // holds the NL average values

//        Dvector2 table() const;             // table of FACTORS x REPORTCOLS

    private:
        void compute(char const *indicatorsFname);  // cpt the f-scores

                                                    // fscores of data[rowIdx]
        void fscores(std::ostream &indicators, 
                     unsigned rowIdx, double const *sdInv);

//        static void difference(Dvector2 &ret, size_t dest, 
//                               size_t left, size_t right);

};
        
inline Dvector2 const &Fscores::scores() const
{
    return d_fscores;
}

#endif


