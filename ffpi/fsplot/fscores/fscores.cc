#include "fscores.ih"

void Fscores::fscores(ostream &indicators, 
                      unsigned rowIdx, double const *sdInv)
{
    auto &ratings = d_data[rowIdx];     // ratings to multiply by sdInv

    for (size_t idx = 0; idx != Enum::N_QUESTIONS; ++idx)
        ratings[idx] *= sdInv[idx];

                                    // for each FFPI dimension (col): compute 
                                    // the inner product of the ratings * 
                                    // weights of that factor
    tuple<unsigned, double> upper[2];
    for (size_t col = 0; col != Enum::N_FACTORS; ++col) 
    {
        double score = inner_product(ratings.begin(), ratings.end(), 
                                     s_weights[col], 0.0);

        d_fscores[rowIdx][col] = score;

                                    //  current value exceeds max.
        if (fabs(score) > fabs(get<1>(upper[0]))) 
        {
            upper[1] = upper[0];        // move max. down
            upper[0] = {col, score};    // next max value
        }                           // if not, does it exceed the next max?
        else if (fabs(score) > fabs(get<1>(upper[1])))
            upper[1] = {col, score};    // replace the next max. value
    }

                                    // write the max factor index, neg/pos
                                    // and the next highest factor index,
                                    //                              neg/pos
    for (auto const &[col, score]: upper)
        indicators << col << ' ' << (score >= 0) << ' ';
    
    indicators << '\n';
}       


