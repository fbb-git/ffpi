#include "fscores.ih"

// with x: scores, S: SD^-1 and W the factor weights fscores f are computed as
//           f = x' S W
// S represented by a vector, computed as:
//          x[i] *= s[i], followed by f = x' * W

void Fscores::compute(char const *indicatorsFname)
{
    auto indicators{ Exception::factory<ofstream>(indicatorsFname) };

    if (d_data.size() == 1)         // 1 row: ideal ratings
        fscores(indicators, 0, s_otherSDinv);
    else
    {
        fscores(indicators, 0, s_selfSDinv);
        fscores(indicators, 1, s_otherSDinv);
    }
}
