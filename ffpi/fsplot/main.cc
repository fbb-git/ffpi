#include "main.ih"

namespace
{
    Arg::LongOption longOptions[] =
    {
        Arg::LongOption{"help", 'h'},
        Arg::LongOption{"ideal", 'i'},
        Arg::LongOption{"version", 'v'},
    };
    auto longEnd = longOptions + size(longOptions);

#include "../basedir.f" // abs. path to the website's base directory

} // anonymous

Options     g_options{ baseDir };
ConfigFile  g_config{ g_options.config(), ConfigFile::RemoveComment };
LogInfo     g_logInfo{ " fsplot " };

// Called as:
//  fsplot 
//         g_language.idx()             1
//         csvPath                      2
//         gnuplotInput                 3
//         gnuplotData                  4
//         d_fScores                    5
//         d_factorIndicators           6

SyslogStream Syslog::s_sls{ "ffpi fsplot" };

int main(int argc, char **argv)
try
{
    Arg &arg = Arg::initialize("hiv", longOptions, longEnd, argc, argv);
    arg.versionHelp(usage, version, 6);

    g_logInfo.setLevel();

    info() << "(V. " << version << ") starts\n";

    Ratings ratings{ argv[2] };             // extract the data from the csv
                                            // file

    Fscores fscores(argv[6], ratings.data(),    // compute the factor scores
                    arg.option('i') ? Enum::IDEAL : Enum::SELF);
 
                                            // create the data files
    DataFiles dataFiles{ fscores.scores() };    
    dataFiles.gnuplotData(argv[4]);
    dataFiles.scoresTable(argv[1][0], argv[5]);

    Gnuplot gnuplot{ ratings, fscores.scores() };
 
    int ret = gnuplot.plot(argv[3]);
 
    return ret;
}
catch (int x)
{
    return Arg::instance().option("hv") != 0 ? 0 : x;
}
catch (exception const &exc)
{
    static char const except[] = "EXCEPTION: ";

    cerr << except << exc.what() << '\n';
    Syslog::log() << except << exc.what() << endl;
    return 1;
}
catch (...)
{
    static char const uncaught[] = "uncaught exception";
    cerr << uncaught << '\n';
    Syslog::log() << uncaught << endl;
    return 1;
}




