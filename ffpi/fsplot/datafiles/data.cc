#include "datafiles.ih"

char const *DataFiles::s_label[Enum::N_LANGUAGES][Enum::N_FACTORS] =
{
    {
        "Extraversie",
        "Mildheid",
        "Ordelijkheid",
        "Emot. Stab.",
        "Autonomie",
    },
    {
        "Extraversion",
        "Mildness",
        "Orderliness",
        "Emot. Stability",
        "Autonomy",
    }
};
