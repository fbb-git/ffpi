#ifndef INCLUDED_DATAFILES_
#define INCLUDED_DATAFILES_

#include "../../support/types/types.h"
#include "../../support/tools/tools.h"

class DataFiles
{
    Dvector2 const &d_fscores;

    static char const *s_label[Enum::N_LANGUAGES][Enum::N_FACTORS];

    public:
        DataFiles(Dvector2 const &fscores);

        void gnuplotData(char const *path) const;
        void scoresTable(int languageIdx, char const *path) const;
};
        
#endif
