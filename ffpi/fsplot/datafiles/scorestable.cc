#include "datafiles.ih"

void DataFiles::scoresTable(int languageIdx, char const *path) const
{
    ofstream out{ Exception::factory<ofstream>(path) };

                                                // show decimal point + 
                                                // trailing zeroes
    out.setf(ios::fixed, ios::floatfield | ios::showpoint);
    out.precision(2);                           // 2 digits behind the .

    ofstream outRaw{ Exception::factory<ofstream>(path + ".raw"s) };
                                                // show decimal point + 
                                                // trailing zeroes
    outRaw.setf(ios::fixed, ios::floatfield | ios::showpoint);
    outRaw.precision(3);                        // 3 digits behind the .

    for (unsigned type = 0, end = d_fscores.size(); type != end; ++type)
    {
        for (size_t factor = 0; factor != Enum::N_FACTORS; ++factor)
            outRaw << setw(7) << d_fscores[type][factor];
        outRaw << '\n';
    }

            // one row per factor, columns are self, other, NLavg 
            // or ideal, NLavg
    for (size_t factor = 0; factor != Enum::N_FACTORS; ++factor)
    {
        out << "    \\makecell[l]{" <<          // label
                    s_label[languageIdx != '0'][factor] << '}';

                                                // f-scores self/.../NL
        for (unsigned type = 0, end = d_fscores.size(); type != end; ++type)
            out <<  " &\n"
                    "    \\makecell[r]{" << d_fscores[type][factor] << '}';

        out << "\n\\\\\n";
    }
}


