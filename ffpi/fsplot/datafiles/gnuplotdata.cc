#include "datafiles.ih"

void DataFiles::gnuplotData(char const *path) const
{
    ofstream out{ Exception::factory<ofstream>(path) };

                                        // visit all factor score types
    for (unsigned type = 0, end = d_fscores.size(); type != end; ++type)
    {
                                        // write all factor scores
        for (size_t factor = 0; factor != Enum::N_FACTORS; ++factor)
        {
            double score = d_fscores[type][factor];
    
            if (score > Enum::MAX_FSCORE)
                score = Enum::MAX_FSCORE;
            else if (score < Enum::MIN_FSCORE)
                score = Enum::MIN_FSCORE;
    
            out << (factor + 1) << ' ' << score << '\n';
        }

        out << "\n\n";
    }
    out.flush();
}
