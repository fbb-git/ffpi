//#define XERR
#include "random.ih"

Random::Random(size_t minimum, size_t maximum, size_t mtSeed)
:
    d_engine(mtSeed)
{
                                            // construct d_dist so that it
                                            // generates random nrs that
    d_dist.param(                           // belong to the required range
        uniform_int_distribution<size_t>::param_type {minimum, maximum}
    ); 
}
