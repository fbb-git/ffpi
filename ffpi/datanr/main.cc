//#define XERR
#include "main.ih"

namespace {

#include "../basedir.f"         // abs. path to the website's base directory

} // anonymous


Options g_options{ g_base };

ostream     m_log{ 0 };
bool        m_debug = false;
ofstream    m_devNull{ 0 };

int main()
try
{
                                            // name of the guard file
    string guard{ g_options.dataPath() + "guard" };

    size_t now = time(0);                   // project times befor now have
                                            // expired

    vector<ProjectData> podVector;                  // stores active projects

                                            // new random nr. (need 'project'
                                            // byond 'try'
    uint32_t project = 10'000 + Tools::random(90'000);

    LockGuard lg{ guard };

    try
    {
        ifstream active = Exception::factory<ifstream>(guard);

        while (true)            
        {
            ProjectData projectInfo;                // next project info
            active.read(reinterpret_cast<char *>(&projectInfo), 
                        sizeof(ProjectData));

            if (active.gcount() != sizeof(ProjectData)) // all info was read
                break;        

            if (projectInfo.expiration < now)   // project expired
                continue;

            podVector.push_back(projectInfo);   // active project
        }
    
        while (
            find_if(podVector.begin(), podVector.end(), // number already 
                    [&](ProjectData const &pod)                 // in use?
                    {
                        return pod.projectNr == project;
                    }
                ) != podVector.end()
        )
            project = 10'000 + Tools::random(90'000);   // then try another
    }
    catch (...)                             // no project file as yet
    {}

    podVector.push_back({ project, 0, now + 15 * 60 }); // add a new project 
                                                    // nr. (valid for 15' 
                                                    // unless accepted)

                                                    // write active project 
                                                    // nrs to the project file
    ofstream out = Exception::factory<ofstream>(guard, ios::out);
    for (auto const &pod: podVector)
        out.write(reinterpret_cast<char const *>(&pod), sizeof(ProjectData));

    cout << project << '\n';
}
catch (...)
{
    cout << "0\n";
    return 1;
}

