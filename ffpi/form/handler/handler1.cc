#include "handler.ih"

Handler::Handler()
:
    d_cgi(false, 0),                        // do not escape received info
                                            // do not do Content-type...
    d_user(d_cgi)
{
    string const &state = d_cgi.param1("state");

    debug() << "Handler starts: state = " << 
                (state.empty() ? "<none>" : state) << endl;

// not used:
//
//    if (state == "report" || state == "getData")    // states using 
//        return;                                     // their own page header

    Tools::textHtml();
}





