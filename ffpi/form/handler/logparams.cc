#include "handler.ih"

void Handler::logParams()
{
    err() << "error-exception. received parameters:" << endl;

    for (auto const &param: d_cgi)
    {
        err() << param.first << ": " << endl;
        for (auto const &value: param.second)
            err() << "  `" << value << endl;
    }
}
