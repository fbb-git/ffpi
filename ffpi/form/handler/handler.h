#ifndef INCLUDED_HANDLER_
#define INCLUDED_HANDLER_

#include <string>
#include <unordered_map>

#include "../../support/types/enum.h"
#include "../../support/syslog/syslog.h"
#include "../../support/options/options.h"

#include "../ffpiuser/ffpiuser.h"

class Handler
{
    FBB::CGI    d_cgi;
    FFPIUser    d_user;

    public:
        Handler();
        ~Handler();
        void process();

    private:
        void logParams();
        void error(Enum::Error enumValue);
};

#endif


