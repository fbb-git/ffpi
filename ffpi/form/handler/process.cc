#include "handler.ih"

void Handler::process()
try
{
    if (d_cgi.param1("state").empty())
        d_user.query(d_cgi.query());        // incoming `other' request
    else 
        d_user.process();                   // or process forms
}
catch (Enum::Error enumValue)
{
    error(enumValue);
}






