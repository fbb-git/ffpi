#include "handler.ih"

void Handler::error(Enum::Error enumValue)
{
    err() << "Caught enumValue " << enumValue << endl;

    logParams();

    DateTime now{ DateTime::LOCALTIME };

    Display display;

    display.out("error.h",
        {
            g_language.message(enumValue),
            now.rfc2822()            
        }
    );
}
