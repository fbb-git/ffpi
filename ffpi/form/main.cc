#include "main.ih"

namespace {

#include "../basedir.f"         // abs. path to the website's base directory

ostringstream s_log;            // to contain this process's log messages

} // anonymous


    // The options are configured in baseDir [active] / etc/ffpi_config
    // ================================================================

Options     g_options{ baseDir };
Language    g_language;
ConfigFile  g_config{ g_options.config(), ConfigFile::RemoveComment };
Mailer      g_mailer;
LogInfo     g_logInfo{ " form " };

SyslogStream Syslog::s_sls{ "ffpi recheck" };

int main(int argc, char **argv)
try
{
    if (argc > 1 and "--version"s == argv[1])
    {
        cout << "V " << version << '\n';
        return 0;
    }

    g_logInfo.setLevel();

    info() << "(V. " << version << ") starts\n";

    Handler handler;                    // Form handling object

    handler.process();                  // process incoming forms
}
catch ([[maybe_unused]] Enum::ForkChild done)
{}                                      // automatic 0 return
catch (Syslog const &syslog)        // Options constructor failed
{
    cerr << "EMERG: Options construction failed, see syslog message\n";
    return 1;
}
catch (exception const &exc)
{
    static char const except[] = "EXCEPTION: ";

    cerr << except << exc.what() << '\n';
    Syslog::log() << except << exc.what() << endl;
    return 1;
}
catch (...)
{
    static char const uncaught[] = "uncaught exception";
    cerr << uncaught << '\n';
    Syslog::log() << uncaught << endl;
    return 1;
}
