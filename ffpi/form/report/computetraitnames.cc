#include "report.ih"

StrVector Report::computeTraitNames() const
{
    ifstream fscores{ Exception::factory<ifstream>(d_fScores + ".raw") };
    
    string adjectiveSelection = g_options.reportsDir() + d_user.nr() + ".txt";
                                        // file containing the adjectives +
                                        // cosines etc selected by various
                                        // procedures. See computeTraits


    StrVector ret{ computeTraits(fscores, adjectiveSelection), "" };

    if (d_type == Enum::STANDARD)
        ret[1] = computeTraits(fscores, adjectiveSelection);

    return ret;
}

// - procedure, per casus en per gem. ander/zelf/ideaal:

// (e) de drie adjectieven met de hoogste som selecteren en cursief invullen op
//  de rapportagepagina's


