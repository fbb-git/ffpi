#include "report.ih"

string Report::dvips(ostream &log) const
{
    string dviFile{ d_pathPrefix      + "dvi" };
    string psFile{ d_pathPrefix      + "ps" };

    string command{ g_config.findKeyTail("dvips:") + ' ' + dviFile +
                                                        " -o " + psFile };

    log << __FILE__ ": dvips " << fname(dviFile) << " -o " <<
                                  fname(psFile) << '\n';
    Tools::childProcess(command);

    return psFile;
}


