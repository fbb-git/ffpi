#include "report.ih"

void Report::checkProject(ostream &log) const
try
{
    log << __FILE__ << ": projectNr = `" << d_projectNr << "'\n";

    stoul(d_projectNr);             // check for active research project

    string outName{ g_options.researchPath() + d_projectNr + ".txt" };
    string rawScores{ d_fScores + ".raw" };

    ifstream fscores{ Exception::factory<ifstream>(rawScores) };
    ofstream out{ Exception::factory<ofstream>(outName, ios::in | ios::ate) };

    out << d_projectNr << ' ' << d_user.ratings();

    string line;                        // only copy the self F-scores
    getline(fscores, line);
    out << line;

    if (not d_user.ppID().empty())
        out << ' ' << d_user.ppID();

    out << endl;
}
catch (...)                 // reached when there's no active research project
{}




