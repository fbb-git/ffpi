#include "report.ih"

// cos(p, a) = a'p / sqrt(p'p * a'a)
// normalizeFscores has changed p so that p'p == 1.
// here a'p is computed for all adjectives in support/language/sadjectives.cc
// (called from 'computetraits.cc')

// static
Dvector Report::cptProjections(Dvector const &fscores)
{
    Dvector ret;

    Adjective const *adj = Language::adjective();

                                    // visit all adjectives
    for (size_t row = 0; row != Language::adjectiveSize(); ++row)
    {
                                    // compute the inner products of the
                                    // (normalized) F-scores and the adjective
                                    // loadings
        double value = inner_product(
                            fscores.begin(), fscores.end(),
                            adj[row].loadings, 0.0);

        ret.push_back(value);
    }

    return ret;                     // return the projections
}
