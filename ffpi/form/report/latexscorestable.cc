#include "report.ih"

string Report::latexScoresTable() const
{
    ifstream scores{ Exception::factory<ifstream>(d_fScores) };

    ostringstream latexScores;
    latexScores << scores.rdbuf();
    
    return latexScores.str();
}
