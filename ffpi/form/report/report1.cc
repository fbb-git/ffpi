#include "report.ih"

Report::Report(FFPIUser const &user, Enum::RatingType type) 
:
    d_pathPrefix(g_options.tmpDir() + user.nr() + '.'),
    d_moldsPrefix(g_options.moldsDir() + g_language.prefix()),
    d_gnuplotEps(d_pathPrefix   + "eps"),

    d_fScores(d_pathPrefix + "fscores"), // .raw for the plain fscores

    d_factorIndicators(d_pathPrefix + "indicators"),
    d_latexInput(d_pathPrefix   + "latex"),

    d_user(user),
    d_projectNr(user.projectNr()),
    d_type(type)
{
    info() << __FILE__ << ": constructing report for " << user.eMail() <<
                                                                    endl;
}





