#include "report.ih"

string Report::headerInfo() const
{
    ostringstream out;
    
    time_t timeValue = time(0);
    tm tmValue{ *localtime(&timeValue) };

    out << 
R"_(    \makecell[l]{)_" << g_language.person() << R"_(:} &
    \makecell[l]{)_" <<  g_language.header(d_type == Enum::IDEAL)<< 
                                                                    R"_(}
\\
    \makecell[l]{)_" << g_language.date() << R"_(:} &
    \makecell[l]{)_" << put_time(&tmValue, "%e %b %Y") << "}\n";

    return out.str();
}
