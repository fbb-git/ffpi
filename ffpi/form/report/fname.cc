#include "report.ih"

//static
char const *Report::fname(string const &path)
{
    size_t pos = path.rfind('/');
    
    return pos == string::npos ? &path[0] : &path[pos + 1];
}
