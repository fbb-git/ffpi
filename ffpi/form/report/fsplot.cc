#include "report.ih"

void Report::fsplot(ostream &log) const
{
    log << __FILE__ " starts\n";

    string gnuplotInput{ d_pathPrefix + "gp" };
    string gnuplotData{ d_pathPrefix  + "dat" };

    DollarText::replace(gnuplotInput, 
                            d_moldsPrefix + s_gnuplot[d_type], 
                            { 
                                d_gnuplotEps, 
                                gnuplotData 
                            } 
                        );

    string csvPath{ d_pathPrefix + "csv" };
    {                                                   // produced CSV file
        ofstream csv{ Exception::factory<ofstream>(csvPath) };
        (this->*s_writeCSV[d_type])(csv);
    }

    string command{ g_options.binDir() + "fsplot "        +
                            g_language.str()        + ' ' +
                            csvPath                 + ' ' +
                            gnuplotInput            + ' ' +
                            gnuplotData             + ' ' +
                            d_fScores               + ' ' +
                            d_factorIndicators  };

    log << __FILE__ ": fsplot " << 
                    g_language.idx() <<         ' ' <<
                    fname(csvPath) <<           ' ' <<
                    fname(gnuplotInput) <<      ' ' <<
                    fname(gnuplotData) <<       ' ' <<
                    fname(d_fScores) <<         ' ' <<
                    fname(d_factorIndicators) << endl;

    Tools::childProcess(command);
}




