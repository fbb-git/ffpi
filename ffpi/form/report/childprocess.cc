#include "report.ih"


// #include <bobcat/syslogstream>              // TMP!!


void Report::childProcess()
{
    prepareDaemon();

    Tools::chdir(g_options.basePath());     // to the active dir. 

    // SyslogStream sls{ "FFPI" };
    // sls << "FFPI STARTS" << endl;

    ostringstream logTxt;                   // log to an ostringstream
    Log log{ logTxt, " report " };

    if (size_t level = g_logInfo.level(); level == Enum::LOG_OFF)
        log.off();
    else
        log.setLevel(level);

    log.level(Enum::INFO);

    if (d_type == Enum::STANDARD)
        log << d_user.name() << " (" << d_user.eMail() << ")\n";
    else
        log << "self/ideal rating for " << d_user.nr() << '\n';

    log.level(Enum::DEBUG);

    // sls << "fsplot starts, d_pathPrefix = " << d_pathPrefix << endl;
    fsplot(log);                        // exec. fsplot

    // sls << "latex starts" << endl;
    latex(log);                         // exec LaTeX 

    // sls << "pdf construction starts" << endl;
    ps2pdf(log, dvips(log));            // exec dvips and then ps2pdf

    log.level(Enum::INFO);

    if (d_type == Enum::STANDARD)
        sendMail(log);

    log << "report construction completed\n";

    log.level(Enum::DEBUG);
    checkProject(log);                  // check existing research project

    cleanup(log);                       // configurable clean up 
                                        //  temporary files
    
    int fd = open(g_options.log().c_str(), O_WRONLY);     // lock the logfile
    flock(fd, LOCK_EX);    

    auto logFile{ Exception::factory<ofstream>(
                            g_options.log(), ios::in | ios::ate) 
                };

    logFile << logTxt.str() << '\n';

    flock(fd, LOCK_UN);

    throw Enum::ForkChild::DONE;    
}


