#include "report.ih"

// static
void Report::sortIndices(Uvector &indices, Dvector const &values)
{
    sort(indices.begin(), indices.end(), 
        [&](size_t lhs, size_t rhs)
        {
            return values[lhs] > values[rhs];
        }
    );
};

