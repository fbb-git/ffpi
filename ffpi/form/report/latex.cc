#include "report.ih"

    // LaTeX produces d_pathPrefix.dvi from d_pathPrefix.latex, produced by
    // fsplot
void Report::latex(ostream &log) const
{
    log << __FILE__ " starts" << endl;

    writeLatexInputFile(log);          // generate tmp/*.latex from its mold

    log << __FILE__ " latex input file constructed" << endl;

    string command = g_config.findKeyTail("latex:") +  " "
                    "--output-directory=" + g_options.tmpDir() + " "
                    "--aux-directory=" + g_options.tmpDir() +    " " +
                    d_latexInput;

    for (size_t idx = 0; idx != 2; ++idx)
    {
        log << __FILE__ ": latex " << fname(d_latexInput) << '\n';
        Tools::childProcess(command);
    }
}


