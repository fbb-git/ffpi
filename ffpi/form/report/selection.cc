#include "report.ih"

// static
void Report::selection(ostream &summary, Uvector const &indices, 
                        Dvector const &projections, 
                        Dvector const &congruences, 
                        uint16_t languageIdx,
                        char const *header)
{
    summary << header;

    for (size_t idx = 0; idx != Enum::N_USE_ADJECTIVES; ++idx)
    {
        uint16_t adjIdx = indices[idx];
        summary << "    " << 
                    Language::adjective()[adjIdx].adjective[languageIdx] <<
                    ", cos = " << projections[adjIdx] << 
                    ", angle = " << round(180 / M_PI * 
                                        acos(projections[adjIdx])) <<
                    ", cos5 =  " << congruences[adjIdx] << 
                    ", angle = " << round(180 / M_PI * 
                                        acos(congruences[adjIdx])) <<
                    '\n';
    }

    summary << '\n';
}
