#include "report.ih"

// static
void Report::writeCSV(ostream &out, vector<double> const &vd)
{
    copy(vd.begin(), vd.end() - 1, ostream_iterator<double>(out, ","));
    out << vd.back() << '\n';
}
