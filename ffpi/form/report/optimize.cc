#include "report.ih"

// determine the indices of the adjectives having the largest sum of
// projections + congruences.

// static
string Report::optimize(Uvector &indices, 
                        Dvector const &projections, 
                        Dvector const &congruences)
{
    Dvector sum;
    for (size_t idx = 0, end = indices.size(); idx != end; ++idx)
        sum.push_back(projections[idx] + congruences[idx]);

    sortIndices(indices, sum);

    return g_language.adjectives(indices);
}
