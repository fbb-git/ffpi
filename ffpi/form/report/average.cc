#include "report.ih"

// static
vector<double> Report::average(string const *otherRatings)
{
    vector<double> vd;

    for (unsigned idx = 0; idx != Enum::N_QUESTIONS; ++idx)
    {
        double avg = 0;

        for (unsigned otherIdx = 0; otherIdx != Enum::N_OTHER; ++otherIdx)
            avg += otherRatings[otherIdx][idx];

        vd.push_back( avg / 3 - '0' - 3);
    }

    return vd;
}
