#include "report.ih"

// cos(p, a) = a'p / sqrt(p'p * a'a)
// F-scores (p) are normalized. the projections contain a'p.


// static
Dvector Report::cptCongruences(Dvector const &projections)
{
    Dvector ret{ projections };

    Adjective const *adj = Language::adjective();

    for (size_t idx = 0; idx != ret.size(); ++idx)
        ret[idx] /= adj[idx].norm;

    return ret;
}
