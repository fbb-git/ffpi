#include "report.ih"

void Report::testRun(char init)
{
    string data[3] = 
    {
        string(Enum::N_QUESTIONS, init),
        string(Enum::N_QUESTIONS, init),
        string(Enum::N_QUESTIONS, init)
    };

    ostringstream csv;

    if (d_type == Enum::SELF)
        writeCSV(csv, string2double(data[0]));        
    else
    {
        writeCSV(csv, string2double(data[0]));        
        writeCSV(csv, average(data));        
    }

    cout << "CSV data:\n" <<
            csv.str() << '\n';
}
