#include "report.ih"

// normalize the 5 F-scores on the file fcores so their SS == 1

// static
Dvector Report::normalizeFscores(istream &fscores)
{
    Dvector ret(Enum::N_FACTORS);

    for (double &value: ret)
        fscores >> value;

    double ss = sqrt(inner_product(ret.begin(), ret.end(), ret.begin(), 0.0));

    for (double &value: ret)
        value /= ss;

//    lg() << __FILE__ << " fscores in: " << rawScores << 
//                                    ", last score: " << ret.back() << endl;

    return ret;    
}
