#include "report.ih"

void Report::sendMail(ostream &out) const
{
    out << g_mailer.sendmail(
                d_user.eMail(),
                g_language.sendmailSubject(),
                DollarText::replaceStream(
                    g_options.mailDir() + 
                        g_language.prefix() + "results", 
                    {
                        d_user.name(),
                        g_config.findKeyTail("reporturl:"),
                        d_user.nr()         // .pdf specified in the mold
                    }
                ),
                false                       // no child process, but calls
                                            // Mailer::mailInserter
            ) << '\n';
}
