//#define XERR
#include "main.ih"

#include "../../../basedir.f" // abs. path to the website's base directory

Options g_options{ g_base };

            // m_ identifiers are only used by main and global functions

ostream     m_log{ 0 };
bool        m_debug = false;
ofstream    m_devNull{ 0 };
Language    g_language;

ConfigFile  g_config{ g_options.config(), ConfigFile::RemoveComment };
Mailer      g_mailer;

int main(int argc, char **argv)
try
{
    if (argc == 1)
    {
        cout << "1st arg: value, 2nd arg: s: self rating, o: other ratings\n";
        return 0;
    }

    CGI cgi;
    FFPIUser dummy(cgi);

    Report report{ dummy, argv[2][0] == 's' ? Enum::SELF : Enum::STANDARD };

    report.testRun(argv[1][0]);
}
catch (...)
{
    return 1;
}
