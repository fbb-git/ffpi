#include "report.ih"

void Report::ps2pdf(ostream &log, std::string const &psFile) const
{
    string pdfFile{ g_options.reportsDir() + d_user.nr() + ".pdf" };

    string command{ g_config.findKeyTail("ps2pdf:") + ' ' + 
                                            psFile + ' ' + pdfFile };

    log << __FILE__ ": ps2pdf " << fname(psFile) << ' ' <<
                                   fname(pdfFile) << endl;
    Tools::childProcess(command);
}


