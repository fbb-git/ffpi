#include "report.ih"

void Report::standardCSV(ostream &out) const
{
    writeCSV(out, string2double(d_user.ratings()) );
    writeCSV(out, average(d_user.otherRatings()) );
}
