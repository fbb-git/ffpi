#include "report.ih"

void Report::cleanup(ostream &log) const
{
                            // keep tmp files and file in data/
    if (g_config.findKeyTail("tmp:") == "keep")
    {
        log << "no tmp cleanup\n";
        return;
    }

    string path{ g_options.tmpDir() + d_user.nr() + ".*" };
    log << "tmp cleanup: " << path << '\n';

    Glob glob{ path };

    for (char const *fname: glob)
        remove(fname);

    if (not d_user.filename().empty())      // mere self/ideal: no data file
        remove(g_options.dataPath() + d_user.filename());
}
