#include "report.ih"

// cos(p, a) = p'a / sqrt(p'p * a'a)
// after normalizing p'p = 1 it drops out of the equation:
// cos(p, a) = p'a / sqrt(a'a)

// Three adjective selection procedures are used (note: a'a is the adjective's
// communality):
//
//  Method 1:
//      cos(p, a) = p'a, in which case 1 - a'a is attributed to the
// adjective's unique variance handling dimension, which doesn't affect p'a,
// since p  has no coordinates on that dimensions, and a'a, including the
// loading on that dimension equals 1: select the three adjectives having the
// largest cos. values
//
//  Method 2:
//      cos(p'a) = p'a / sqrt(a'a), computing the cos. between p and a in the
// 5 FFPI factor space, : select the three adjectives having the largest
// cos. values 
//
//  Method 3:
//      the std. method, in which cos + p'a is used as selection measure


// static
string Report::computeTraits(istream &in, string const &summaryFname)
{
    ofstream summary{ Exception::factory<ofstream>
                                (
                                    summaryFname,
                                    ios::in | ios::out, 
                                    ios::in | ios::out | ios::trunc
                                )
                    };
    summary.seekp(0, ios::end);         // open the summary file to its end

    Dvector scores{ normalizeFscores(in) };             // SS f-scores = 1
        // F-scores are normalized so the division by sqrt(p'p) can be 
        // omitted when congruences are computed.

    Dvector projections = cptProjections(scores);       // a'p for all adj.
    Dvector congruences = cptCongruences(projections);  // computes (p'p == 1:
                                                        // a'p / sqrt(a'a) )

    Uvector indices(projections.size());
    iota(indices.begin(), indices.end(), 0);        // indices [0..nAdj)

    uint16_t languageIdx = g_language.idx();

        // Method 1: sort the projections

    sortIndices(indices, projections);
    selection(summary, indices, projections, congruences, languageIdx,
        "Adjectives selected using the 5 FFPI + Unique (adjective) "
        "dimensions:\n"
        "(1st pair of values)\n");

    sortIndices(indices, congruences);
    selection(summary, indices, projections, congruences, languageIdx,
        "Adjectives selected using the 5 FFPI dimensions:\n"
        "(2nd pair of values)\n");

    string ret = optimize(indices, projections, congruences);

    selection(summary, indices, projections, congruences, languageIdx,
        "Adjectives selected using the std. FFPI procedure\n"
        "(proj. + congr, sum of both cos. values)\n"); 

    return ret;
}


