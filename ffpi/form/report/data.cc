#include "report.ih"

void (Report::*Report::s_writeCSV[])(ostream &csv) const
{
    &Report::standardCSV,           // 2 sets of CSV values
    &Report::selfCSV,               // pure self
    &Report::selfCSV,               // ideal, both use self-ratings
};

unsigned Report::s_nIndicators[] = {8, 4, 4};   // required #indicators

char const *Report::s_gnuplot[] = 
{
    "gnuplot",          // STANDARD
    "gnuplot.self",     // SELF
    "gnuplot.ideal"     // IDEAL
};

char const *Report::s_latex[] = 
{
    "latex",          // STANDARD
    "latex.self",     // SELF
    "latex.ideal"     // IDEAL
};

