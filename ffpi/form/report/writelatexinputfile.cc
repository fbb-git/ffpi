#include "report.ih"

void Report::writeLatexInputFile(ostream &log) const
{
    log << __FILE__ << ' ' << d_latexInput << endl;

    StrVector traits = computeTraitNames(); 

    DollarText::replace(
                d_latexInput, d_moldsPrefix + s_latex[d_type], 
                { 
                    headerInfo(),                       // $0
                    d_gnuplotEps,                       // $1
                    latexScoresTable(),                 // $2
                    traits[0],                          // $3
                    traits[1],                          // $4
                }
            );

}

// Below the log stmnt:
//
//    auto indicators{ Exception::factory<ifstream>(d_factorIndicators) };
//
//        // self:    max factor, pos/neg, next factor, pos/neg, 
//        // other:   max factor, pos/neg, next factor, pos/neg, 
//        // neg is 0, pos is 1
//
//    vector<unsigned> indicator;
//    copy(
//        istream_iterator<unsigned>{ indicators }, 
//        istream_iterator<unsigned>{}, 
//        back_inserter(indicator)
//    );
//
//    if (indicator.size() != s_nIndicators[d_type])
//        throw Enum::LATEXINPUT;
//
//    indicator.resize(s_nIndicators[Enum::STANDARD]);
// 
// //    if (log)
// //    {
// //        log << __FILE__ << " indicator values:";
// //        for( unsigned value: indicator)
// //            log << ' ' << value;
// //        log << endl;
// //    }


//    DollarText::replace(
//                d_latexInput, d_moldsPrefix + s_latex[d_type], 
//                { 
//                    headerInfo(),                       // $0
//                    d_gnuplotEps,                       // $1
//                    latexScoresTable(),                 // $2
//
//                                            // facet info self
//                    g_language.factorLabel(indicator[0], indicator[1]), // $3
//                    g_language.factorLabel(indicator[2], indicator[3]), // $4
//
//                        // typering according to g_language.facet:
//                    g_language.facet(indicator[0], indicator[1],        // $5
//                                     indicator[2], indicator[3]),
//
//                                        // facet info other
//                                        // not used for RatingType::SELF
//                                        //          and RatingType::IDEAL
//                    g_language.factorLabel(indicator[4], indicator[5]), // $6
//                    g_language.factorLabel(indicator[6], indicator[7]), // $7
//
//                        // typering according to g_language.facet:
//                    g_language.facet(indicator[4], indicator[5],        // $8
//                                     indicator[6], indicator[7]),
//                }
//            );




