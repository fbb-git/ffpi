#include "report.ih"

// static
vector<double> Report::string2double(string const &selfData)
{
    vector<double> vd;

    for (uint16_t score: selfData)
        vd.push_back(score - '0' - 3);

    return vd;
}
