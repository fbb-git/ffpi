#include "ffpiuser.ih"

// static
string FFPIUser::addProject(string const &userEmail, Date const &endDate)
{
    string research{ g_options.dataPath() + "research" };

    LockGuard guard{ research };
    
    auto projects{ readProjects(research) };            // get the projects

    uint32_t projectNr;                                 // determine new
    do                                                  // project nr.
    {
        projectNr = 10'000 + Tools::random(90'000);
    }
    while (projects.find(projectNr) != projects.end());

    string iv = Tools::iv();                           
    string encrypted = Tools::encrypt(iv, userEmail);

    ProjectData pod{ static_cast<uint32_t>(encrypted.size()), 
                     projectNr, g_language.idx(), 
                     static_cast<uint16_t>(endDate.year), 
                     static_cast<uint8_t>(endDate.month) };

    ofstream data = 
            Exception::factory<ofstream>(research, ios::in | ios::out);

    data.seekp(0, ios::end);

    Tools::writeN(data, &pod);              // write the project data
    Tools::writeS(data, iv);                // write the encryption IV
    Tools::writeS(data, encrypted);         // and the encrypted data

    info() << __FILE__ <<": data size now  " << data.tellp() << "\n"
                       "wrote project " << pod.projectNr() << 
            ", year: " << pod.year() << 
            ", month: " << static_cast<uint16_t>(pod.month()) << 
            ", email: " << userEmail << 
            ", file state: " << data.good() << endl;

    return to_string(projectNr);
}
