#include "ffpiuser.ih"

// receives: userEmail, name, gender, e0, e1, e2

void FFPIUser::verify()
{
//    setLanguage();

    d_userEmail = param("userEmail");
    d_name      = param("name");
    d_gender    = param("gender");
    d_other[0]  = param("e0");
    d_other[1]  = param("e1");
    d_other[2]  = param("e2");

//    lg() <<  __FILE__ << "user: " << d_userEmail << 
//                        ", name: " << d_name << 
//                        ", gender: " << d_gender << 
//                        ", other0: " << d_other[0] << 
//                        ", other1: " << d_other[1] << 
//                        ", other2: " << d_other[2] << 
//                        endl;

    if (
        not eMail(d_userEmail)           or
        d_name.empty()                       or
        "MF"s.find(d_gender) == string::npos or
        not eMail(d_other[0])                or
        not eMail(d_other[1])                or
        not eMail(d_other[2])
    )
    {
        d_display.out("missingfields.h");
        return;
    }

    d_filename = Tools::md5hashText(d_userEmail);

    if (get())                              // data already defined ?
    {
        d_display.out("alreadydefined.h");
        return;
    }

    d_data.write(d_filename, toString());   // write the binary data, 
                                            // converting info to string using
                                            // the member toString 

    selfRatings(Enum::STANDARD, "selfinstructions.h", g_language.areYou(), 
                g_language.close());
}
