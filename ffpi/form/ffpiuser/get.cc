#include "ffpiuser.ih"

bool FFPIUser::get()
{
    info() <<  __FILE__" userEmail: " << d_userEmail  << endl;

    string data;

    info() <<  __FILE__" md5hash fname: " << d_filename  << endl;

    if (not d_data.read(&data, d_filename))
        return false;

    read(data);                                 // convert string to data
    return true;
}




