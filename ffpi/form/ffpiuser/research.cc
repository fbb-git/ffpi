#include "ffpiuser.ih"

// receives: userEmail, name, month, year and projectNr

void FFPIUser::research()
try
{
    DateTime now{ DateTime::LOCALTIME };            // get the current date

    string userEmail = d_cgi.param1("userEmail");

    Date endDate{0, 0};
    try
    {
        endDate = Tools::next(                      // get the next month/year
                    Date{
                        stoul(d_cgi.param1("year")),// which is when the
                        stoul(d_cgi.param1("month"))// project's data 
                    }
                );                                  // collection ends
    }               
    catch (...)                                     // incorrect/unavailable
    {}                                              // values

    info() << "FFPIUser::research: email: " << userEmail << 
            ", month: " << endDate.month << ", year: " << endDate.year << endl;

    if (
        isHistory(endDate, now)         // end date at/before this month
        or
        not validRange(endDate, now)    // or end year not in 10-year range
        or
        not validEmail(userEmail)       // or invalid e-mail address
    )
        throw Enum::INVALID_DATA;

    string projectNr = addProject(userEmail, endDate);

    info() << "FFPIUser::research: projectNr: " << projectNr << endl;

                                        // create the research data file
    Exception::factory<ofstream>(g_options.researchPath() +
                                    projectNr + ".txt");
    d_display.out("research.h",
        {
            projectNr,
            g_language.month(endDate.month),
            to_string(endDate.year),
            userEmail,
            g_config.findKeyTail("projecturl:")
        }
    );    

    debug() << g_mailer.sendmail(
            userEmail,
            g_language.mailSubject(2),
            DollarText::replaceStream(
                g_options.mailPath() + g_language.prefix() +"research", 
                {
                    projectNr,                         // $0
                    g_language.month(endDate.month),
                    to_string(endDate.year),
                    g_config.findKeyTail("projecturl:") // $3
                }
            )
        ) << endl;
}
catch (...)
{
    throw;
}





