#include "ffpiuser.ih"

void FFPIUser::ideal()
{
    d_display.append("idNr", d_nr);         // store d_nr in the form

    selfRatings(Enum::RatingType::IDEAL, 
                "idealinstructions.h", g_language.isIdeal(),
                g_language.closeMereSelf());
}
