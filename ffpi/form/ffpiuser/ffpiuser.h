#ifndef INCLUDED_FFPIUSER_
#define INCLUDED_FFPIUSER_

#include <iosfwd>
#include <unordered_map>

#include "../../support/types/enum.h"
#include "../../support/types/types.h"
#include "../../support/display/display.h"
#include "../../support/datastore/datastore.h"

namespace FBB
{
    class CGI;
    class DateTime;
};

class FFPIUser
{
//    friend std::ostream &operator<<(std::ostream &out, FFPIUser const &data);

    enum Mode
    {
        SELF,
        OTHER           // + idx
    };

    typedef std::unordered_map<std::string, void (FFPIUser::*)()>   Map;
    typedef void (FFPIUser::*CompletedHandler)(std::string const &, unsigned);

    FBB::CGI &d_cgi;

    Display d_display;
    DataStore d_data;               // ffpiusers' data
    std::string d_filename;         // name of the file containing the data

    std::string d_selfData;
    std::string d_otherData[3];
    bool d_oneOther;                // only one other data collection req'ed

    std::string d_nr;               // ID nr when retrieving the report.
                                    // set in answered.cc ffpiuser1.cc
    std::string d_userEmail;
    std::string d_name;
    std::string d_gender;
    std::string d_other[3];

    std::string d_nq;               // actual nr of questions being asked

    static Map s_state;             // maps state names to handling functions

    static CompletedHandler s_completed[];
    static size_t const s_completedSize;

    public:
        FFPIUser(FBB::CGI &cgi);

        void process();
        void query(std::string const &query);

        std::string const &nr() const;
        std::string const &name() const;
        std::string const &eMail() const;
        std::string const &filename() const;
        std::string const &ratings() const;
        std::string const *otherRatings() const;

        std::string projectNr() const;
        std::string ppID() const;

    private:
        void project();                     // data entry research subjects
        void research();                    // research project requested
        void verify();
        void self();                        // merely self-ratings
        void ideal();                       // merely ideal-ratings

        void answered();

        std::string toString() const;       // convert FFPIUser's data to string
                                            // read the data (true) setting
        bool get();                         // read data from d_filename

                                            // get the CGI parameter 'name'
        std::string param(std::string const &name) const;

        void read(std::string const &data); // 

        void selfRatings(Enum::RatingType type, char const *file, 
                         char const *opening, char const *closing,
                         std::string const &projectNr = "",
                         std::string const &ppID = "");

        void selfCompleted(std::string const &ratings, unsigned type);
        void otherCompleted(std::string const &ratings, unsigned idx);

        void inviteOther(size_t idx) const;
        void otherPage(size_t idx);
        void other();

        void checkCompleted();                      // also: create report
        bool otherData();                           // check oneOther or all

        void selfIdealReport(Enum::RatingType type);    // SELF/IDEAL

        void setLanguage();

                                            // fill ratings beyond d_nq with 
                                            // '3' values if data/nq was 
                                            // specified
        std::string completed(std::string const &ratings) const;

        static bool eMail(std::string const &address);          // .ih
        static std::string nQuestions();    // #questions to administer
        static std::string uniqueNr();      // unique nr for tmpDir() files

        static bool isHistory(Date endDate, FBB::DateTime const &now);
        static bool validRange(Date date, FBB::DateTime const &now);
        static bool validEmail(std::string const &userEmail);

        static std::string addProject(std::string const &userEmail, 
                                      Date const &endDate);

        static std::unordered_map<uint32_t, ProjectData> readProjects(
                                                std::string const &guard);

        static bool knownProject(std::string const &projectNr);
};

inline std::string FFPIUser::projectNr() const
{
    return param("projectNr");
}

inline std::string FFPIUser::ppID() const
{
    return param("ppID");
}

inline std::string const &FFPIUser::nr() const
{
    return d_nr;
}

inline std::string const &FFPIUser::name() const
{
    return d_name;
}

inline std::string const &FFPIUser::filename() const
{
    return d_filename;
}

inline std::string const &FFPIUser::eMail() const
{
    return d_userEmail;
}

inline std::string const &FFPIUser::ratings() const
{
    return d_selfData;
}

inline std::string const *FFPIUser::otherRatings() const
{
    return d_otherData;
}

#endif
