#include "ffpiuser.ih"

void FFPIUser::query(string const &query)
{
    debug() << __FILE__ << ". Query = `" << query << '\'' << endl;

    unsigned qIdx = query.length() - 1;

    g_language.setLanguage(query.back() == '0' ? "" : "en/");

    debug() << __FILE__ <<  " language idx: " << g_language.idx() << 
                            " language prefix: `" << 
                            g_language.prefix() << '\'' << endl;

    uint16_t idx = query[--qIdx] - '0';
    if (idx >= Enum::N_OTHER)              // other index must be in [0..N)
        throw Enum::NO_QUERY;

    d_filename = query;
    d_filename.resize(--qIdx);              // rm the trailing check chars:
                                            // e-mail char, user idx, language

                                            // lock the data
    LockGuard guard{ d_data.path() + d_filename };

                                            // get the user's data, then check
                                            // whether the 1st char of the
                                            // other's e-mail == query's 
                                            // char beyond the filename
    if (not get() or d_other[idx][0] != query[d_filename.length()])
        throw Enum::NO_QUERY;              // if not: leave

    otherPage(idx);
}

