#include "ffpiuser.ih"

string FFPIUser::completed(string const &ratings) const
{
    string ret{ ratings };

    size_t nq = stoul(d_nq);

    fill(ret.begin() + nq, ret.end(), '3');

    if (nq != Enum::N_QUESTIONS)
        debug() << "completed ratings: " << ret << '\n';

    return ret;
}
