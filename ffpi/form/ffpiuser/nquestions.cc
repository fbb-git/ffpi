#include "ffpiuser.ih"

// static
string FFPIUser::nQuestions()
{
    ifstream nqStream{ g_options.dataPath() + "nq" };

    size_t nq = Enum::N_QUESTIONS;

    nqStream >> nq;

    debug() << "nQuestions: " << nq << endl;

    return to_string(
                nq > Enum::N_QUESTIONS ? Enum::N_QUESTIONS : nq
            );
}
