#include "ffpiuser.ih"

//static
bool FFPIUser::knownProject(string const &projectNr)
try
{
    uint32_t project = stoul(projectNr);        // get the numeric value

    string research{ g_options.dataPath() + "research" };

    size_t now = Tools::asNumber( DateTime{ DateTime::LOCALTIME } );

    LockGuard guard{ research };
    
    auto projects{ readProjects(research) };    // get the projects

    auto iter = projects.find(project);         // find project 'projectNr'

    return 
        iter == projects.end() ?            // no such project
            false
        :
            now < Tools::asNumber(
                        Date{ iter->second.year(), iter->second.month() }
                  );
                                            // 'now' before end-date: true
                                            //  (date OK, project exists)
}
catch (...)
{
    return false;                           // 'projectNr' isn't a number
}


