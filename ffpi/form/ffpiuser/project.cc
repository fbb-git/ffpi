#include "ffpiuser.ih"

// receives: projectNr, ppID

void FFPIUser::project()
{
    string projectNr = param("projectNr");
    string ppID      = param("ppID");

    info() <<  __FILE__ " project: " << projectNr << 
                        " ppID: `" << ppID << "'\n";

    if (not knownProject(projectNr))            // also checks expiration date
    {
        d_display.out("unknownproject.h",
            {
                projectNr
            }
        );
        return;
    }

    // see self.cc: idNr not used, and not provided

    selfRatings(Enum::SELF, "instructionsonlyself.h", g_language.areYou(), 
                g_language.closeMereSelf(), projectNr, ppID);
}
