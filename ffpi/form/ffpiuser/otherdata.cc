#include "ffpiuser.ih"

bool FFPIUser::otherData()
{
    if (not d_oneOther)
    {
        for (string const &otherData: d_otherData)      // all answers must be
        {                                               // available
            if (otherData[0] == 0)
                return false;
        }
    
        return true;
    }

    info() << __FILE__ << ": one other rating suffices" << endl;

    string *begin = d_otherData;                // one vector is OK

    string *answered = find_if(begin, &d_otherData[3],
        [&](string const &str)
        {
            return str[0] != 0;                 // find first answered
        }
    );

    if (answered == &d_otherData[3])                // none found
    {
        info() << __FILE__ << ": no other ratings available yet" << endl;
        return false;
    }


    for (; begin != answered; ++begin)      // assign first empty vectors
        *begin = *answered;
                                            // assign remaining vectors
    for (begin = answered + 1; begin != &d_otherData[3]; ++begin)
        *begin = *answered;

    info() << __FILE__ << ": forced completion of all other ratings" << endl;
    return true;
}


