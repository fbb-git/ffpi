#include "ffpiuser.ih"

void FFPIUser::selfIdealReport(Enum::RatingType type)   
{
    Report report{  *this, type };
    report.generate();
}
