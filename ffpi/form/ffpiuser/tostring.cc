#include "ffpiuser.ih"

// The data are written first. Since they have fixed formats they can be 
// overwritten in-place, without having to rerite the full stream. 
string FFPIUser::toString() const
{
    ostringstream out;                      // encrypt confidential data

    out <<  d_nr        << '\n' <<
            d_userEmail << '\n' <<
            d_name      << '\n' <<
            d_gender    << '\n' <<
            d_other[0]  << '\n' <<
            d_other[1]  << '\n' <<
            d_other[2]  << '\n';

    string iv = Tools::iv();
    string encrypted = Tools::encrypt(iv, out.str());

    out.str("");                            // convert client info to string

    Tools::writeS(out, d_selfData);         // write the ratings

    for (string const &ratings: d_otherData)
        Tools::writeS(out, ratings);

    Tools::writeS(out, iv);                 // write the encryption IV

    uint16_t size = encrypted.size();       // write size of encrypted data
    Tools::writeN(out, &size);

    Tools::writeS(out, encrypted);          // and the data themselves    


    return out.str();                       // return the binary string    
}




