#include "ffpiuser.ih"

FFPIUser::FFPIUser(CGI &cgi)
:
    d_cgi(cgi),
    d_display(cgi),                         // reference to the CGI object
    d_data(g_options.dataPath()),
    d_selfData(Enum::N_QUESTIONS, 0),
    d_oneOther(g_config.findKeyTail("oneOther:") == "true"),
    d_nr(uniqueNr()),                       // initial nr, may be reset by
                                            // read()
    d_nq(nQuestions())
{
    setLanguage();

    for (string &ratings: d_otherData)
        ratings.resize(Enum::N_QUESTIONS);
}
