#include "ffpiuser.ih"

void FFPIUser::otherPage(size_t idx)        // idx = 0, 1, 2
{
    info() << __FILE__ " other = " << idx << ", file = " << d_filename << 
                                                                        endl;

    if (d_otherData[idx][0] != 0)         // ratings already
        throw Enum::DATA_AVAILABLE;      // available.


    d_display.append("mode",        to_string(OTHER));    
    d_display.append("ratingType",  to_string(idx));    
    d_display.append("filename",    d_filename);    
    d_display.append("state",       "other");
    d_display.append("language",    g_language.prefix());

    Enum::Gender gender = d_gender == "M" ? Enum::MALE : Enum::FEMALE;

    d_display.out("otherinstructions.h", 
        {
            "Is",
            d_name,
            g_language.himHer(gender),
            g_language.hisHer(gender),
             g_language.heShe(gender),
            g_language.close(),
            g_language.otherThanks(),
            d_nq,
        }
    );
}




