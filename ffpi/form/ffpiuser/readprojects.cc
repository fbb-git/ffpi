#include "ffpiuser.ih"

// static
unordered_map<uint32_t, ProjectData> FFPIUser::readProjects(
                                                    string const &research)
{
    unordered_map<uint32_t, ProjectData> ret;

    ifstream projects{ research };

    ProjectData pod;
    while (Tools::readN(projects, &pod) == 1)       // read 1 POD
    {
        projects.seekg(Enum::IV_SIZE + pod.encryptSize(), ios::cur);
        ret.insert({ pod.projectNr(), pod });
    }

    return ret;
}
