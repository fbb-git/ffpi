#include "ffpiuser.ih"

void FFPIUser::selfRatings(Enum::RatingType type, char const *file, 
                           char const *opening, char const *closing,
                           string const &projectNr, string const &ppID)
{
    debug() << __FILE__ " " << type << ", filename = " << d_filename << 
            ", next file: " << file << ", language `" << 
            g_language.prefix() << "'\n";

    Enum::Gender gender = d_gender == "M" ? Enum::MALE : Enum::FEMALE;

    d_display.append("mode", to_string(Mode::SELF));
    d_display.append("ratingType", to_string(type));
    d_display.append("filename", d_filename);
    d_display.append("language", g_language.prefix());

    d_display.out(file, 
        {
            opening,
            g_language.himHer(gender),
            g_language.hisHer(gender),
            g_language.heShe(gender),
            closing,
            g_language.selfThanks(),
            d_nq,
            projectNr,
            ppID
        }
    );
}
