#include "ffpiuser.ih"

// static
bool FFPIUser::isHistory(Date endDate, DateTime const &now)
{
        // data collection interval has already passed if the end date
        // is before or at (<=) the current month/year

    return Tools::asNumber(endDate) <= Tools::asNumber(now);
}
