#include "ffpiuser.ih"

FFPIUser::Map FFPIUser::s_state =
{
    { "answered",   &FFPIUser::answered },      // handle received answers
    { "ideal",      &FFPIUser::ideal    },
    { "other",      &FFPIUser::other    },
    { "project",    &FFPIUser::project  },
    { "research",   &FFPIUser::research },
    { "self",       &FFPIUser::self     },
    { "verify",     &FFPIUser::verify   },
};

FFPIUser::CompletedHandler FFPIUser::s_completed[] =
{
    &FFPIUser::selfCompleted,             // Mode: SELF
    &FFPIUser::otherCompleted,            // Mode: OTHER
};

size_t const FFPIUser::s_completedSize = size(s_completed);

