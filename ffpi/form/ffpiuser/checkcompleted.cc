#include "ffpiuser.ih"

void FFPIUser::checkCompleted()
{
    if 
    (                                               // verify that self-data
        d_selfData[0] == 0                          // are available
        or
        not otherData()                             // and all other data
    )
        return;

    info() << __FILE__ ": data collection " << d_userEmail << " completed." << 
                                                                    endl;

    Report report{ *this, Enum::STANDARD };
    report.generate();
}







