#include "ffpiuser.ih"

void FFPIUser::otherCompleted(string const &ratings, unsigned idx)
{
    debug() << __FILE__ "\n";

    if (idx >= Enum::N_OTHER || d_otherData[idx][0] != 0)
        throw Enum::DATA_AVAILABLE;

    d_otherData[idx] = completed(ratings);

    d_data.write(d_filename, toString());

    d_display.out("alldone.h");

    checkCompleted();   
}
