#include "ffpiuser.ih"

void FFPIUser::setLanguage()
{
    g_language.setLanguage(param("language"));

    debug() << __FILE__ <<  " param: `" << param("language") << 
                        "' language idx: " << g_language.idx() << 
                        " language prefix: `" << 
                            g_language.prefix() << '\'' << endl;
}
