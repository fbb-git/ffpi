#include "ffpiuser.ih"

// static
bool FFPIUser::validEmail(string const &userEmail) 
{
    return 
        userEmail.length() >= 3 
        and
        count_if(userEmail.begin(), userEmail.end(), 
            [&](char ch)
            {
                return ch == '@';
            }
        ) == 1;
}
