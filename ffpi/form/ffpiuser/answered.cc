#include "ffpiuser.ih"

void FFPIUser::answered()
{
    string idNr{ d_cgi.param1("idNr") };                // 0: not used

    unsigned mode = stoul(d_cgi.param1("mode"));        // 0, 1
    unsigned type = stoul(d_cgi.param1("ratingType"));  // 0..2 for mode 1:
                                                        // other raters
                // 0..2 for mode 0: 0: std self,
                //                  1: mere self (and project-self),
                //                  2: ideal

    info() << __FILE__ " mode = " << mode << ", type = " << type <<
                       ", idNr = " << idNr << '\n';
  

    if (idNr != "0")
        d_nr = idNr;                // reset d_nr to the form's (= original)
                                    // idNr

    if (mode == 1 or type == 0)     // other ratings or plain self ratings
    {
        d_filename = param("filename");
    
        LockGuard guard{ d_data.path() + d_filename };   // lock the data
    
        if (not get())          // no data found, shouldn't happen,
            return;             // in that case: show an empty page.
    }

    std::string const &ratings = d_cgi.param1("ratings");

    debug() << __FILE__ " ratings: " << ratings << '\n' <<
            ", completedSize = " << s_completedSize << '\n';

//    ratings = string(Enum::N_QUESTIONS, ratings[0]);
//    debug() << __FILE__ " tmp converted ratings: " << ratings << endl;

    if (mode >= s_completedSize)
        throw Enum::DATA_AVAILABLE;

                                    // self-, otherCompleted
    (this->*s_completed[mode])(ratings, type);

    debug() << __FILE__ " completed\n";
}


