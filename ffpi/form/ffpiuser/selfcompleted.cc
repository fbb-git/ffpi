#include "ffpiuser.ih"

void FFPIUser::selfCompleted(string const &ratings, unsigned type)
{
    debug() << __FILE__ "\n";

    if (type != Enum::STANDARD)         // mere self or ideal
    {
        d_selfData = completed(ratings);    // store the self-ratings

        d_display.out("selfidealdone.h",
            {
                g_config.findKeyTail("reporturl:"),
                d_nr
            }
        );

        debug() << __FILE__ << " Project: " << param("projectNr") << ", "
                "ID: `" << param("ppID") << endl;
                               
        selfIdealReport(static_cast<Enum::RatingType>(type));
        return;
    }


    if (d_selfData[0] != 0)             // data already available
        throw Enum::DATA_AVAILABLE;

    d_selfData = ratings;               // store the self-ratings

    d_data.write(d_filename, toString());

    for (size_t idx = 0; idx != Enum::N_OTHER; ++idx)
        inviteOther(idx);

    d_display.out("alldone.h");
}
