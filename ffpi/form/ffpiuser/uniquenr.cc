#include "ffpiuser.ih"

// static
string FFPIUser::uniqueNr()
{
    while (true)
    {
                                        // try a random nr.
        string ret{ to_string(1000 + Tools::random(9000)) };

        if (exists(g_options.reportsDir() + ret + ".pdf"))
            continue;

        return ret;
    }   
}
