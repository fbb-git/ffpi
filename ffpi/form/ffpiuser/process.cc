#include "ffpiuser.ih"

void FFPIUser::process()
{
    debug() << __FILE__ " state = " << d_cgi.param1("state") << 
                    ", language prefix: `" << g_language.prefix() << "'\n";

    auto iter = s_state.find(d_cgi.param1("state"));    // find the state

    if (iter == s_state.end())
        throw Enum::NO_STATE;       // handled by Handler::process

    (this->*(iter->second))();      // execute the appropriate state:
                                    // see ffpi/README.fsa
    debug() << __FILE__ " state " << d_cgi.param1("state") << " completed\n";
}
