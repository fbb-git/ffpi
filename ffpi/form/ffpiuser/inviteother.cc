#include "ffpiuser.ih"

void FFPIUser::inviteOther(size_t idx) const
{
    info() << "inviteOther: e-mail: " << d_userEmail << ": " << idx <<
            " (" << d_other[idx] << ')' << endl;

    string url{ g_config.findKeyTail("otherurl:")  + d_filename + 
                d_other[idx][0] + to_string(idx) + g_language.str() };

    info() << "inviteOther: url: " << url << endl;
            
    debug() << g_mailer.sendmail(
            d_other[idx],
            g_language.mailSubject(0) + d_name + g_language.mailSubject(1),
            DollarText::replaceStream(
                g_options.mailPath() + g_language.prefix() 
                                                            +"inviteother", 
                {
                    d_name,                                 // $0
                    url                                     // $1
                }
            )
        ) << endl;
}
