#include "ffpiuser.ih"

void FFPIUser::read(std::string const &data)
{
    istringstream in(data);                  // read the data


    Tools::readS(in, d_selfData, Enum::N_QUESTIONS);   // read the self-

    for (string &ratings: d_otherData)                  // and other-ratings
        Tools::readS(in, ratings, Enum::N_QUESTIONS);

    string iv;
    Tools::readS(in, iv, Enum::IV_SIZE);               // read the IV

    uint16_t size;                          // read size of encrypted data
    Tools::readN(in, &size);

    string decrypted;
    Tools::readS(in, decrypted, size);          // then the encrypted data

    decrypted = Tools::decrypt(iv, decrypted);  // and decrypt

    in.str(decrypted);                      // obtain the confidential data

    getline(in, d_nr);
    getline(in, d_userEmail);
    getline(in, d_name);
    getline(in, d_gender);

    for (string &other: d_other)            // other addresses
        getline(in, other);
}
