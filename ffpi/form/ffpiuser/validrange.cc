#include "ffpiuser.ih"

// static
bool FFPIUser::validRange(Date date, DateTime const &now)
{
    return now.year() <= date.year and date.year <= now.year() + 10;
}
