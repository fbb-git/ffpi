#include "main.ih"

namespace
{

char const info[] = 
R"(
Provide the alfacom.txt filename as argument. The files adjectives.txt and 
loadings.txt must be present. 
The generated array of support/types/types.h Adjective structs is written 
to cout.
Use the ../alfacom/ program to check for multiply occurring translations.
)";

char const head[] = 
R"(//#define XERR
#include "language.ih"

//static 
Adjective const Language::s_adjective[] = 
{
)";

char const tail[] = 
R"(};

//static 
size_t Language::s_adjectiveSize = size(s_adjective);)";

}   // anon. namespace

int main(int argc, char **argv)
try
{
    if (argc == 1)
    {
        cout << info << '\n';
        return 0;
    }

    ifstream adjectives{ Exception::factory<ifstream>("adjectives.txt") };
    ifstream loadings{   Exception::factory<ifstream>("loadings.txt")   };
    ifstream alfacom{    Exception::factory<ifstream>(argv[1])          };

    cout << head;
    cout.setf(ios::fixed, ios::floatfield);

    size_t lineNr = 0;
    while (true)
    {
        string adjective;

        if (not readAdjective(adjectives, &adjective))
            break;

        ++lineNr;

        Adjective adj;
        if (not readLoadings(loadings, &adj))
            throw Exception{} << "At loadings line " << lineNr << 
                                 ": loadings missing for " << adjective;

        if (not readAdj(alfacom, &adj))
            throw Exception{} << "At " << argv[1] << " line " << lineNr << 
                                 ": Adjective data missing for " << adjective;

        if (adjective != adj.adjective[Enum::NL])
            throw Exception{} << "At " << argv[1] << " line " << lineNr << 
                                 ": Adjective " << adj.adjective[Enum::NL] <<
                                        " should be " << adjective;

        if (adj.adjective[Enum::EN].empty())    // no translation available
            continue;

        write(adj);
    }

    cout << tail << '\n';                  // end the Adj. array
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "unexpected exception\n";
    return 1;
}







