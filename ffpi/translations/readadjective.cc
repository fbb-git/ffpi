#include "main.ih"

bool readAdjective(istream &in, string *adj)
{
    string line;
    if (not getline(in, line))
        return false;

    *adj = String::trim(line);
    return true;
}
