#include "main.ih"

void write(Adjective const &adj)
{
    cout.precision(5);

    cout << "    { " << setw(7) << adj.comm << ", " <<
                        setw(6) << adj.norm << ", {";

    cout.precision(3);
    
    for (size_t idx = 0; idx != Enum::N_FACTORS - 1; ++idx)
        cout << setw(7) << adj.loadings[idx] << ',';

    cout << setw(7) << adj.loadings[Enum::N_FACTORS - 1] << " }, { \"" << 
                        adj.adjective[Enum::NL] << "\", \"" <<
                        adj.adjective[Enum::EN] << "\" } },\n";
}

