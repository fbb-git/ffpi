#include "main.ih"

bool readLoadings(istream &in, Adjective *adj)
{
    string line;
    if (not getline(in, line))
        return false;

    istringstream ins{ line };

    double norm = 0;
    double value;
    for (size_t idx = 0; idx != Enum::N_FACTORS; ++idx)
    {
        ins >> value;
        adj->loadings[idx] = value;
        norm += value * value;
    }

    adj->norm = sqrt(norm);

    return not ins.fail();
}
