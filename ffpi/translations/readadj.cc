#include "main.ih"

bool readAdj(istream &in, Adjective *adj)
{
    char ch;

    in >> adj->comm >> ch >>                    // read up to the English xlat
                    adj->adjective[Enum::NL];

    string line;              
    if (not getline(in, line))                  // get the rest of theline
        return false;                           // none? then error

                                                // trim the English xlat
    adj->adjective[Enum::EN] = String::trim(line);

    return true;
}
