set terminal eps color  size 33 cm, 22 cm

# $ 0: path of the .eps file to create. 
set output "$0"

load 'molds/gp.frame'

load 'molds/en/gp.labels'

# $ 1: path of the data file. 
plot '$1' \
          index 0 with linespoints ls 2 title 'Self' at 0.26,0.91,\
     ''   index 1 with linespoints ls 3 title 'NL' at 0.79,0.91

