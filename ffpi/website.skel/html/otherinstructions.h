    <script src="/fjs/addhidden.js"></script>
    <script src="/fjs/questions.js"></script>
    <script src="/fjs/instructions.js"></script>
    <script>
        sessionStorage.opening      = "$0 $1";     // is [name client] 
        sessionStorage.name         = "$1";
        sessionStorage.himHer       = "$2";
        sessionStorage.hisHer       = "$3";
        sessionStorage.heShe        = "$4";
        sessionStorage.closing      = "$5";
        sessionStorage.thanks       = "$6";
        sessionStorage.nQuestions   = "$7";
    </script>
</head>

<body class="full"> 

<!--
    onsubmit="alert('otherinstructions.h');return false"
-->

<form name=form
onsubmit="return instructions('/ffpi/otherinstructions.shtml')"
>

<div class="container">

<b>Algemene instructie</b><p>


De hierna volgende vragenlijst bestaat uit 100 vragen en eindigt met de optie
&#x2018;antwoorden opslaan&#x2019;.
<p> 

Uw antwoorden worden pas opgeslagen wanneer u na het beantwoorden van de
laatste vraag &#x2018;antwoorden opslaan&#x2019; hebt aangeklikt. 
<p>

Wanneer u tussentijds stopt gaan uw inmiddels gegeven
antwoorden verloren.  U wordt dan ook verzocht om alle vragen in een keer te
beantwoorden.  Het beantwoorden van de vragen duurt ongeveer 15 minuten.

<p>
U blijft zelf anoniem in dit onderzoek. Uw antwoorden worden niet aan $1
doorgegeven. Uit uw antwoorden en die van twee anderen, die eveneens
de vragenlijst invullen, wordt een gemiddelde berekend. Dit gemiddelde 
en de betekenis ervan wordt aan $1 ter beschikking gesteld.

<p>

<input type="submit" value="verder">

