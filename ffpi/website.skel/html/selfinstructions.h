    <script src="/fjs/addhidden.js"></script>
    <script src="/fjs/questions.js"></script>
    <script src="/fjs/instructions.js"></script>
    <script>
        sessionStorage.opening      = "$0";     // is deze persoon; bent u
        sessionStorage.himHer       = "$1";
        sessionStorage.hisHer       = "$2";
        sessionStorage.heShe        = "$3";
        sessionStorage.closing      = "$4";
        sessionStorage.thanks       = "$5";
        sessionStorage.nQuestions   = "$6";
    </script>
</head>

<body class="full"> 
<form name="form" action="/fbin/form" 
        onsubmit="return instructions('/ffpi/question.html')"
      method="post">     <!-- instructions.js -->       
<div class="container">

<b>Algemene instructie</b><p>

De FFPI bestaat uit 100 vragen. Nadat de laatste vraag is beantwoord 
kunnen uw antwoorden worden opgeslagen door op 
&#x2018;antwoorden opslaan&#x2019; te klikken.

<p>
Wanneer u tussentijds de afname afbreekt worden uw inmiddels gegeven
antwoorden niet opgeslagen.
<p>

Klik op &#x2018;beginnen&#x2019; om met de afname van de FFPI te beginnen.

<p>

<input type="submit" value="beginnen">






