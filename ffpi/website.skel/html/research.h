    <script src="/js/preamble.js"></script>
</head>

<body class="full"> 

<nav class="top-menu-markup">                                               
    <ul class="spread">
        <li class="li-linewise">
            <a href="#" title="ffpi home-page" 
                onclick= "return toPage('/ffpi')"
            >
            <img border=0 src="/fimages/home.png" height=16em >
            </a>
        </li>

        <li class="li-linewise">
            <a href="#" title="De FFPI"
                onclick="return toPage('/ffpi/ffpi.html')"
            >
                De FFPI
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Privacyverklaring"
                onclick="return toPage('/ffpi/privacy.html')"
            >
                Privacy
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Contact"
                onclick= "return toPage('/ffpi/contact.html')"
            >
                Contact
            </a>
        </li>
    </ul>                                                                   
</nav>                                                                      


<div class="container">

<h1 class=center-text>
    De <em>Five-Factor Personality Inventory III</em><br>
    <div class=subhead> 
        (Hendriks, Hofstee, De Raad, &amp; Brokken, 2019)
    </div>
</h1>

Het door u aangevraagde project is geactiveerd.<br>
De einddatum van uw project is 01 $1 $2<p>

Bewaar de volgende informatie s.v.p.:
<ul>
<li>Uw FFPI-project heeft projectcode <font color='blue'>$0</font>

<li> De data van uw project komen beschikbaar in het bestand
    <a href="$4/$0.txt"> $4/$0.txt</a>. Dit bestand kunt u te allen tijde t/m
    een maand na de einddatum van het project downloaden (via <em>Save Page
    As...</em>). 

<li> Geef uw proefpersonen de projectcode (en optioneel een
    proefpersoon-identficatie) en verwijs hen naar de pagina <font
    color='blue'>https://ffpi.nl/project.shtml</font>.

<li> Op deze pagina dienen uw proefpersonen de projectcode en eventueel hun
    proefpersoon-identificatie in te vullen, waarna zij de vragen van de FFPI
    kunnen beantwoorden.  
</ul>

<p>

De verzamelde data worden per proefpersoon in &eacute;&eacute;n regel
opgeslagen. Deze regels bevatten de volgende informatie:
<ul>
<li> kolommen 1 t/m 5: de projectcode;

<li> kolommen 7 t/m 106: 100 antwoorden op de vragen van de FFPI (niet door
    spaties gescheiden), 1: nee t/m 5: ja,

<li> kolommen 108 t/m 141: de vijf door een spatie van elkaar gescheiding
    factorscores (formaat: -d.ddd gevolgd door een spatie; bij positieve
    factorscores is het minteken vervangen door een spatie.)

<li> vanaf kolom 143 (indien gebruikt): de proefpersoonidentificatie 
</ul>

U ontvangt een kopie van bovenstaande informatie op e-mail adres $3<br>
Het onderwerp (Subject) van de e-mail is: <em>Uw FFPI
onderzoeksprojectnummer</em>. 

<p>
Zodra de einddatum van uw project is bereikt ontvangt u een e-mail. De dan
verzamelde onderzoeksgegevens kunnen dan nog gedurende een maand worden
gedownload, waarna ze uit ons systeem worden verwijderd.


</div>

<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>
</body></html>


