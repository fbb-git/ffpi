    <script src="/fjs/addhidden.js"></script>
    <script src="/fjs/standard.js"></script>
    <script src="/fjs/topage.js"></script>
</head>

<body class="full"> 

<form name=form action="/fbin/form" onsubmit="return standard(1)" method=post>
    <script src="/fjs/topage.js"></script>

<nav class="top-menu-markup">                                               
    <ul class="spread">
        <li class="li-linewise">
            <a href="#" title="ffpi home-page" 
            onclick= "return toPage('/ffpi')"
            >
            <img border=0 src="/fimages/home.png" height=16em >
            </a>
        </li>

        <li class="li-linewise">
            <a href="#" title="De FFPI"
                onclick="return toPage('/ffpi/ffpi.html')"
            >
                De FFPI
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Privacyverklaring"
                onclick= "return toPage('/ffpi/privacy.html')"
            >
                Privacy
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Contact"
                onclick= "return toPage('/ffpi/contact.html')"
            >
                Contact
            </a>
        </li>
    </ul>                                                                   
</nav>                                                                      


<div class="container">


<h1>Gebruikergegevens</h1>

<!--
Voor eventueel
benodigde verdere toelichting op deze pagina, zie het
&#x2018;gebruiksprotocol&#x2019; onder &#x2018;Documenten&#x2019;
<p>
-->

Vul hier uw eigen gegevens in (uitsluitend voor gebruik in de communicatie met
u, c.q. tussen u en de drie anderen): 
<p>


<table class=lightgrey style="border-collapse: collapse;">
<tr>
    <td><div style='margin-left:5em'></div></td>
</tr>
<tr>
    <td></td>
    <td>
        <table style="border-collapse: collapse;">
        <tr>
            <td class=right>Uw e-mailadres: </td> 
            <td ><input class=margin type=email 
                        name=userEmail size=50></td>
        </tr>
        <tr class=darkrow>
            <td class=right>Uw naam (zoals afgesproken met de anderen):
            </td>  
            <td ><input class=margin type=text name=name 
                    pattern="^['\w.\s]+$" size=50></td>
        </tr>
        <tr class=right>
            <td class=right>Geslacht: </td>
            <td> 
                <table>
                <tr><td>
                    <input id=gender1 type=radio name=gender value=M> M
                </td></tr>
                <tr><td>
                    <input id=gender2 type=radio name=gender value=F> V
                </td></tr>
            </table>
            </td>
        </tr>
        <tr class=darkrow>
            <td colspan=2>
                De e-mailadressen van de drie anderen: </td>
            <td> 
        </tr>
        <tr class=darkrow>
            <td></td>
            <td>
            <table>
            <tr>
                <td>1.</td>
                <td ><input class=margin type=email name=e0 size=50></td>
            </tr>
            <tr>
                <td>2.</td>
                <td ><input class=margin type=email name=e1 size=50></td>
            </tr>
            <tr>
                <td>3.</td>
                <td ><input class=margin type=email name=e2 size=50></td>
            </tr>
            </table>
            </td>
        </tr>
        </table>
    </td>
</tr>
</table>

<p>

<div class=spread>
        <input type="submit" value="ga naar de vragenlijst" onclick="nr = 1">
</div>
