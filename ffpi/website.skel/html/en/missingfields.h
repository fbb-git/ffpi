    <script src="/fjs/addhidden.js"></script>
    <script src="/fjs/standard.js"></script>
    <script src="/fjs/topage.js"></script>
</head>

<body class="full"> 

<form name=form action="/fbin/form" onsubmit="return standard(1)" method=post>
    <script src="/fjs/topage.js"></script>

<nav class="top-menu-markup">                                               
    <ul class="spread">
        <li class="li-linewise">
            <a href="#" title="ffpi home-page" 
            onclick= "return toPage('/ffpi/en')"
            >
            <img border=0 src="/fimages/home.png" height=16em >
            </a>
        </li>

        <li class="li-linewise">
            <a href="#" title="De FFPI"
                onclick="return toPage('/ffpi/en/ffpi.html')"
            >
                The FFPI
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Privacy statement"
                onclick= "return toPage('/ffpi/en/privacy.html')"
            >
                Privacy
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Contact"
                onclick= "return toPage('/ffpi/en/contact.html')"
            >
                Contact
            </a>
        </li>
    </ul>                                                                   
</nav>                                                                      


<div class="container">


<h1>Gebruikergegevens</h1>

Please provide your own details here (to be used only to communicate with
you, c.q. for communication between you and your other raters): 
<p>


<table class=lightgrey style="border-collapse: collapse;">
<tr>
    <td><div style='margin-left:5em'></div></td>
</tr>
<tr>
    <td></td>
    <td>
        <table style="border-collapse: collapse;">
        <tr>
            <td class=right>Your email address: </td> 
            <td ><input class=margin type=email 
                        name=userEmail size=50></td>
        </tr>
        <tr class=darkrow>
            <td class=right>Your name (as agreed with the others):
            </td>  
            <td ><input class=margin type=text name=name 
                    pattern="^['\w.\s]+$" size=50></td>
        </tr>
        <tr class=right>
            <td class=right>Gender: </td>
            <td> 
                <table>
                <tr><td>
                    <input id=gender1 type=radio name=gender value=M> M
                </td></tr>
                <tr><td>
                    <input id=gender2 type=radio name=gender value=F> V
                </td></tr>
            </table>
            </td>
        </tr>
        <tr class=darkrow>
            <td colspan=2>
                The e-mail addresses of three others: </td>
            <td> 
        </tr>
        <tr class=darkrow>
            <td></td>
            <td>
            <table>
            <tr>
                <td>1.</td>
                <td ><input class=margin type=email name=e0 size=50></td>
            </tr>
            <tr>
                <td>2.</td>
                <td ><input class=margin type=email name=e1 size=50></td>
            </tr>
            <tr>
                <td>3.</td>
                <td ><input class=margin type=email name=e2 size=50></td>
            </tr>
            </table>
            </td>
        </tr>
        </table>
    </td>
</tr>
</table>

<p>

<div class=spread>
        <input type="submit" value="to the questionnaire" onclick="nr = 1">
</div>
