</head>

<body class="full"> 
<form>
<div class="container">

<h1>FFPI temporarily unavailable</h1>

The FFPI is currently unavailable to you: the email address you entered has
already been registered.
<p>

Within one week after completion of the FFPI, you will receive an email with
a link to the report. Once this link has been received, you will be able to
complete the FFPI once more, if you would wish to do so.
<p>

It is, however, possible to complete the FFPI once more right away if you
would have a different email address available that you can use. In that case
please click on the button below <em>usage data</em>.
<p>

By clicking the button <em>ffpi homepage</em> you will return to the
ffpi-homepage. 
<p>

<div class=lightgrey>
<table>
<tr>
<td>
    <input type="submit" value="usage data" 
        onclick="window.open('/ffpi/en/standard.html', '_self'); return false;">
</td>
<td>
    <input type="submit" value="ffpi homepage" 
        onclick="window.open('/ffpi/en', '_self'); return false;">
</td>
</table>
</div>
