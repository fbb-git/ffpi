    <script src="/fjs/addhidden.js"></script>
    <script src="/fjs/questionsen.js"></script>
    <script src="/fjs/instructions.js"></script>
    <script>
        sessionStorage.opening      = "$0 $1";     // is [name client] 
        sessionStorage.name         = "$1";
        sessionStorage.himHer       = "$2";
        sessionStorage.hisHer       = "$3";
        sessionStorage.heShe        = "$4";
        sessionStorage.closing      = "$5";
        sessionStorage.thanks       = "$6";
        sessionStorage.nQuestions   = "$7";

        sessionStorage.language = 'en';
    </script>
</head>

<body class="full"> 

<form name=form
      onsubmit="return instructions('/ffpi/en/otherinstructions.shtml')"
>

<div class="container">

<b>General instructions</b><p>


The questionnaire that follows consists of 100 items, ending with the option
&#x2018;save responses&#x2019;.
<p> 

Your responses are only saved if you click on the button
&#x2018;save responses&#x2019; once you have completed the questionnaire. 
<p>

If you quit the session before completion, any answers provided will be
deleted. You are therefore requested to complete all items in one go.
This will take approximately 15 minutes.

<p>
You will remain anonymous. Your answers will not be shown to $1.
Based on your answers and those of two others, who were also invited to
complete the questionnaire, an average profile will be calculated. Only this 
average profile and its meaning will be made available to $1.

<p>

<input type="submit" value="continue">

