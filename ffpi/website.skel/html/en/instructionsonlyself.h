    <script src="/fjs/addhidden.js"></script>
    <script src="/fjs/questionsen.js"></script>
    <script src="/fjs/instructions.js"></script>
    <script>
        sessionStorage.opening      = "$0";     // is deze persoon; bent u
        sessionStorage.himHer       = "$1";
        sessionStorage.hisHer       = "$2";
        sessionStorage.heShe        = "$3";
        sessionStorage.closing      = "$4";
        sessionStorage.thanks       = "$5";
        sessionStorage.nQuestions   = "$6";
        sessionStorage.projectNr    = "$7";
        sessionStorage.ppID         = "$8";

        sessionStorage.language = 'en';
    </script>
</head>

<body class="full"> 
<form name="form" action="/fbin/form" 
        onsubmit="return instructions('/ffpi/en/question.html')"
      method="post">
<div class="container">

<b>General instructions</b><p>

The FFPI consists of 100 items. After completion, a report will be generated 
when you click on the
&#x2018;make the report&#x2019; button.

<p>
If you quit the session before completion, any answers provided will be
deleted.
<p>

Click &#x2018;start&#x2019; to start completing the FFPI.

<p>

<input type="submit" value="start">






