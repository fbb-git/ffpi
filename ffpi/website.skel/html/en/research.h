    <script src="/js/preamble.js"></script>
</head>

<body class="full"> 


<nav class="top-menu-markup">                                               
    <ul class="spread">
        <li class="li-linewise">
            <a href="#" title="ffpi home-page" 
                onclick= "return toPage('/ffpi/en')"
            >
            <img border=0 src="/fimages/home.png" height=16em >
            </a>
        </li>

        <li class="li-linewise">
            <a href="#" title="The FFPI"
                onclick="return toPage('/ffpi/en/ffpi.html')"
            >
                The FFPI
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Privacy statement"
                onclick= "return toPage('/ffpi/en/privacy.html')"
            >
                Privacy
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Contact"
                onclick= "return toPage('/ffpi/en/contact.html')"
            >
                Contact
            </a>
        </li>
    </ul>                                                                   
</nav>                                                                      


<div class="container">

<h1 class=center-text>
    De <em>Five-Factor Personality Inventory III</em><br>
    <div class=subhead> 
        (Hendriks, Hofstee, De Raad, &amp; Brokken, 2019)
    </div>
</h1>

The project you requested is now active.<br>
Your project's end-date is 01 $1 $2<p>

Please save the following details about your project:
<ul>
<li>Your FFPI-project has project code <font color='blue'>$0</font>

<li> Your project's data become available in the file 
    <a href="$4/$0.txt"> $4/$0.txt</a>. You can download this file (via
    <em>Save Page As...</em>)any time until one month after the project's
    end-date.

<li> Pass the project code (and optionally a subject identification) to your
subjects and refer them to the page <font
    color='blue'>https://ffpi.nl/en/project.shtml</font>.

<li> At that page your subjects must enter the project code (and optionally
    their subject identification), after which they can answer the questions of
    the FFPI.
</ul>

<p>

The data obtained from subjects are stored in one line per subject. Each line
contains the following information:
<ul>
<li> columns 1 through 5: the project code;

<li> columns 7 through 106: 100 answers to the questions of the FFPI (not
    separated by blanks), 1: no through 5: yes,

<li> columns 108 t/m 141: the five factor scores separated by blanks 
    (format: -d.ddd followed by a blank; positive factor 
    scores have their minuses replaced by blanks.)

<li> starting at column 143 (if used): the subject identification
</ul>

You will receive a copy of the above information at e-mail address $3<br>
The subject of that e-mail is: <em>Your FFPI research project number</em>. 

<p>
Once the end-date of your project has been reached you will receive an
e-mail. Data collected until that date can be downloaded for another month,
after which they are removed from our system.


</div>

<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>
</body></html>


