    <script src="/fjs/topage.js"></script>
</head>

<body class="full"> 
<form>
<div class="container">

<nav class="top-menu-markup">                                               
    <ul class="spread">
        <li class="li-linewise">
            <a href="#" title="ffpi home-page" 
            onclick= "return toPage('/ffpi')"
            >
            <img border=0 src="/fimages/home.png" height=16em >
            </a>
        </li>

        <li class="li-linewise">
            <a href="#" title="De FFPI"
                onclick="return toPage('/ffpi/ffpi.html')"
            >
                De FFPI
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Privacyverklaring"
                onclick= "return toPage('/ffpi/privacy.html')"
            >
                Privacy
            </a>
        </li>
        <li class=li-linewise>
            <a href="#" title="Contact"
                onclick= "return toPage('/ffpi/contact.html')"
            >
                Contact
            </a>
        </li>
    </ul>                                                                   
</nav>                                                                      

<p>
    Het rapport  wordt op dit moment  geconstrueerd. Dat duurt ongeveer 10
seconden. 
<p>

    Het rapport kan via
        <a href="$0/$1.pdf" target="_blank">deze link</a>
    worden bekeken of gedownload. Wanneer het niet verschijnt, wacht dan een
paar seconden en open de link dan nogmaals.
    
<p>
    
    Het rapport wordt na een week vernietigd.  Tot die tijd kunt u het
downloaden van deze lokatie: 
    <a href="$0/$1.pdf" target="_blank">$0/$1.pdf</a>.
    



