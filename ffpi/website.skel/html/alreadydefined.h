</head>

<body class="full"> 
<form>
<div class="container">

<h1>FFPI tijdelijk niet bruikbaar</h1>

De FFPI is momenteel niet bruikbaar: het door u vermelde e-mail adres is
al geregistreerd.
<p>

Bij normaal gebruik krijgt u binnen een week nadat u de FFPI heeft ingevuld
een e-mail met informatie waar de rapportage kan worden
gedownload. Vanaf dat moment kunt u de FFPI desgewenst opnieuw invullen.
<p>

Het is ook mogelijk om nu al een nieuwe afname van de FFPI te starten wanneer
u over een extra e-mailadres beschikt. Klik in dat geval op onderstaande knop 
knop <em>gebruiksgegevens</em>.
<p>

Door op de knop <em>ffpi homepage</em> te klikken keert u terug naar de
ffpi-homepage. 
<p>

<div class=lightgrey>
<table>
<tr>
<td>
    <input type="submit" value="gebruiksgegevens" 
        onclick="window.open('/ffpi/standard.html', '_self'); return false;">
</td>
<td>
    <input type="submit" value="ffpi homepage" 
        onclick="window.open('/ffpi', '_self'); return false;">
</td>
</table>
</div>
