var incomplete = 
[
    'U kunt verder zodra u het projectnummer heeft ingevuld',
    'You may continue once you have provided the project number'
];

function project(language) 
{
    var form = document.forms["form"];

    var projectNr = form['projectNr'].value;

    if (projectNr  == "") 
    {
        alert(incomplete[language == 'en/' ? 1 : 0]);
        return false;
    }

    addHidden('projectNr',  projectNr);
    addHidden('ppID',       form['ppID'].value);
    addHidden('language',   language);

    addHidden('state',      'project');
} 








