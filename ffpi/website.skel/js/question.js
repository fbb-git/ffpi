//  sessionStorage.idx = 0;
//  sessionStorage.nQuestions = form['nQuestions'].value;
//  sessionStorage.questions = questions array
//  sessionStorage.ratings = Array(questions.length);

//  sessionStorage.heShe = "hij" or "zij";      _heShe_
//  sessionStorage.himHer = "hem" or "haar";    _himHer_
//  sessionStorage.hisHer = "zijn" or "haar";   _hisHer_

//  sessionStorage.projectNr: empty or projectNr (see
//                                                form/ffpiuser/project.cc)
//  sessionStorage.ppID: empty or pp-ID (optional when projectNr is available.

var nQuestions = 0;
var idx = 0;

function questionValidate()
{
    addHidden('state',      'answered');

    addHidden('ratings',    sessionStorage.ratings);
    addHidden('filename',   sessionStorage.filename);
    addHidden('ratingType', sessionStorage.ratingType);
    addHidden('mode',       sessionStorage.mode);
    addHidden('idNr',       sessionStorage.idNr);
    addHidden('projectNr',  sessionStorage.projectNr);
    addHidden('ppID',       sessionStorage.ppID);

//    if (sessionStorage.language === undefined)
//        sessionStorage.language = 'nl';

    addHidden('language',   sessionStorage.language);
    
    document.form.submit();
}

function questionAnswer(value)
{
    var ratings = sessionStorage.ratings;

    sessionStorage.ratings = 
        ratings.substr(0, idx) + value + ratings.substr(idx + 1);

    if ((1 + idx) < nQuestions)
        ++sessionStorage.idx;

    window.location.reload();
}

function questionForward()
{
    if (idx + 1 >= nQuestions)
        submit();
    else
    {
        ++sessionStorage.idx;
        window.location.reload();
    }
}

function questionBackward()
{
    --sessionStorage.idx;
    window.location.reload();
}

var languageText = [
        ["maak het rapport",    "make the report"],
        ["antwoorden opslaan",  "save responses"]
    ];
            
function questionVisibility()
{
    var form = document.forms["form"];

    form.reset();

    idx = Number(sessionStorage.idx);
    nQuestions = Number(sessionStorage.nQuestions);

    var answer = Number(sessionStorage.ratings[idx]);

    var lastDivVisible = 'hidden';

    if (answer >= 1 && answer <= 5)
    {
        var langIdx = sessionStorage.language == '' ? 0 : 1;
        var typeIdx = 
                sessionStorage.mode == '0'  // not mere self/ideal rating
                &&
                sessionStorage.ratingType != '0' ? 0 : 1;


        if (1 + idx >= nQuestions)
        {
            form.elements['qsubmit'].value = 
                languageText[typeIdx][langIdx];

            lastDivVisible = 'visible';
        }
        
        document.getElementById("nr" + answer).checked = true;
    }
    document.getElementById('lastDiv').style.visibility = lastDivVisible;

    if (idx == 0)
        document.getElementById('backID1').setAttribute('disabled', '');
    else
        document.getElementById('backID1').removeAttribute('disabled');

    if (answer == 0 || idx + 1 == nQuestions)
        document.getElementById('forwardID1').setAttribute('disabled', '');
    else
        document.getElementById('forwardID1').removeAttribute('disabled');
}

    





