var nr;

var incomplete = 
[
    'U kunt verder zodra alle velden zijn ingevuld',
    'You may continue once you have filled out all fields'
];

var isHistory = 
[
    ' is in het verleden...',
    ' lies in the past...'
];

function research(language) 
{
    var form = document.forms["form"];

    var date = new Date();
    var year = form['year'].value;
    var month = form['month'].value;

    if (year == date.getFullYear() && month < date.getMonth())
    {
        alert((month + 1) + '-' + year + ' ' + 
                isHistory[language == 'en/' ? 1 : 0]);
        return false;
    }
    
    var email = form['userEmail'].value;

    if (email  == "") 
    {
        alert(incomplete[language == 'en/' ? 1 : 0]);
        return false;
    }

    addHidden('userEmail',  email);
    addHidden('language',   language);
    addHidden('month',      month);
    addHidden('year',       year);

    addHidden('state',      'research');
} 

function months()
{
    for (var idx = 0; idx != 12; ++idx)
    {
        var fmt = idx + 1;
        fmt = ("0" + fmt).slice(-2);        
        document.write("<option value=" + idx + ">" + fmt + "</option>\n");
    }
}

function years()
{
    var year = new Date().getFullYear();

    for (var idx = 0; idx != 10; ++idx)
        document.write("<option value=" + 
                (year + idx) + ">" + (year + idx) + "</option>\n");
}








