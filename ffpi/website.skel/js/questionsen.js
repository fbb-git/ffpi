var questions = [
    "takes time out to chat",                               //  1. 
    "gets angry easily",                                    //  2. 
    "acts without planning",                                //  3. 
    "feels desperate",                                      //  4. 
    "does as he/she is told",                               //  5. 
    "gets the party going",                                 //  6. 
    "goes out of his/her way for others",                   //  7. 
    "loves order and regularity",                           //  8. 
    "thinks that all will be well",                         //  9. 
    "echoes what others say",                               // 10. 
    "keeps in the background",                              // 11. 
    "inquires about others' well-being",                    // 12. 
    "likes to follow a regular schedule",                   // 13. 
    "invents problems for himself/herself",                 // 14. 
    "decides things on his/her own",                        // 15. 
    "feels uncomfortable",                                  // 16. 
    "takes others' interests into account",                 // 17. 
    "leaves his/her belongings around",                     // 18. 
    "is able to see the best in a situation",               // 19. 
    "takes charge",                                         // 20. 
    "speaks softly",                                        // 21. 
    "waits for his/her turn",                               // 22. 
    "does things that others find strange",                 // 23. 
    "is afraid that he/she will do the wrong thing",        // 24. 
    "will believe anything",                                // 25. 
    "has a lot of fun",                                     // 26. 
    "accepts people as they are",                           // 27. 
    "does crazy things",                                    // 28. 
    "keeps his/her emotions under control",                 // 29. 
    "is easily deterred",                                   // 30. 
    "avoids company",                                       // 31. 
    "sees to his/her own needs first",                      // 32. 
    "does things at the last minute",                       // 33. 
    "has a dark outlook on the future",                     // 34. 
    "takes risks",                                          // 35. 
    "loves large parties",                                  // 36. 
    "tries to prevent quarrels",                            // 37. 
    "accomplishes his/her work on time",                    // 38. 
    "fears for the worst",                                  // 39. 
    "lets others make the decisions",                       // 40. 
    "loves to chat",                                        // 41. 
    "starts fights",                                        // 42. 
    "behaves properly",                                     // 43. 
    "readily overcomes setbacks",                           // 44. 
    "looks at things from different angles",                // 45. 
    "is apprehensive about new encounters",                 // 46. 
    "respects others' feelings",                            // 47. 
    "works according to a routine",                         // 48. 
    "can stand a great deal of stress",                     // 49. 
    "takes the initiative",                                 // 50. 
    "cheers people up",                                     // 51. 
    "uses others for his/her own ends",                     // 52. 
    "seeks danger",                                         // 53. 
    "bursts into tears",                                    // 54. 
    "does what others want him/her to do",                  // 55. 
    "keeps apart from others",                              // 56. 
    "takes an interest in other people's lives",            // 57. 
    "wants everything to add up perfectly",                 // 58. 
    "looks at the bright side of life",                     // 59. 
    "is easily intimidated",                                // 60. 
    "starts conversations",                                 // 61. 
    "tells tall stories about himself/herself",             // 62. 
    "leaves his/her work undone",                           // 63. 
    "can take his/her mind off his/her problems",           // 64. 
    "waits for others to lead the way",                     // 65. 
    "keeps others at a distance",                           // 66. 
    "likes to do things for others",                        // 67. 
    "does things by the book",                              // 68. 
    "knows how to control himself/herself",                 // 69. 
    "follows the crowd",                                    // 70. 
    "prefers to be alone",                                  // 71. 
    "imposes his/her will on others",                       // 72. 
    "works hard",                                           // 73. 
    "is sure of his/her ground",                            // 74. 
    "reacts quickly",                                       // 75. 
    "acts comfortably with others",                         // 76. 
    "wants to be in charge",                                // 77. 
    "is easily distracted",                                 // 78. 
    "panics easily",                                        // 79. 
    "meets challenges",                                     // 80. 
    "lives in a world of his/her own",                      // 81. 
    "does most of the talking",                             // 82. 
    "keeps his/her appointments",                           // 83. 
    "is down in the dumps",                                 // 84. 
    "knows what he/she wants",                              // 85. 
    "falls silent when strangers are around",               // 86. 
    "respects the opinions of others",                      // 87. 
    "likes to be well prepared",                            // 88. 
    "loses his/her temper",                                 // 89. 
    "is easy to fool",                                      // 90. 
    "makes friends easily",                                 // 91. 
    "orders people around",                                 // 92. 
    "does unexpected things",                               // 93. 
    "worries about things",                                 // 94. 
    "is full of ideas",                                     // 95. 
    "radiates joy",                                         // 96. 
    "demands to be the center of interest",                 // 97. 
    "neglects his/her duties",                              // 98. 
    "keeps a cool head",                                    // 99. 
    "engages in discussions",                               //100. 
];

