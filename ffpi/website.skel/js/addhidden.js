function addHidden(key, value) 
{
                    // element exists? then reassign its value
    if (document.getElementsByName(key).length != 0)    
        document.getElementsByName(key)[0].value = value;
    else        
    {               // element does not exit: define a new key/value pair
        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = key; 
        input.value = value;
        
        document.forms['form'].appendChild(input);
    }
}
