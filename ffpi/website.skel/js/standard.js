var nr;

var incomplete = 
[
    'U kunt pas verder nadat alle velden zijn ingevuld',
    'You may continue once you have filled out all fields'
];

function standard(language) 
{
    var form = document.forms["form"];
    var ok = 1;

    if (form['userEmail'].value  == "") 
        ok = 0;
    
    if (form['name'].value == "")
        ok = 0;
    
        // cf. https://stackoverflow.com/questions/25387081/
        //           javascript-read-radio-button-value-in-ie-and-firefox
    if (form['gender'][0].checked)
        form['gender'].value = form['gender'][0].value;
    else if (form['gender'][1].checked)
        form['gender'].value = form['gender'][1].value;
    else
        ok = 0;

    if (
            form['e0'].value == "" ||
            form['e1'].value == "" ||
            form['e2'].value == ""
    )
        ok = 0;
    
    if (ok == 0)
    {
        alert(incomplete[language == 'en/' ? 1 : 0]);
        return false;
    }

    addHidden('state', 'verify');
    addHidden('language', language);
} 


