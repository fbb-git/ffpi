var questions = [
    "tijd neemt voor een praatje",                          //  1. 
    "gauw boos wordt",                                      //  2. 
    "dingen doet zonder planning",                          //  3. 
    "ten einde raad is",                                    //  4. 
    "doet wat hem/haar gezegd wordt",                       //  5. 
    "de stemming erin brengt",                              //  6. 
    "zich inspant voor anderen",                            //  7. 
    "van orde en regelmaat houdt",                          //  8. 
    "denkt dat alles wel goed komt",                        //  9. 
    "anderen napraat",                                      // 10. 
    "zich op de achtergrond houdt",                         // 11. 
    "vraagt hoe het met iemand gaat",                       // 12. 
    "graag volgens schema werkt",                           // 13. 
    "zichzelf problemen aanpraat",                          // 14. 
    "beslist op eigen verantwoordelijkheid",                // 15. 
    "zich slecht op zijn/haar gemak voelt",                 // 16. 
    "rekening houdt met belangen van anderen",              // 17. 
    "spullen laat slingeren",                               // 18. 
    "altijd wel een lichtpuntje ziet",                      // 19. 
    "de leiding neemt",                                     // 20. 
    "zacht praat",                                          // 21. 
    "op zijn/haar beurt wacht",                             // 22. 
    "dingen doet die anderen vreemd vinden",                // 23. 
    "bang is iets verkeerd te doen",                        // 24. 
    "zich alles laat wijsmaken",                            // 25. 
    "zich opperbest amuseert",                              // 26. 
    "mensen accepteert zoals ze zijn",                      // 27. 
    "gekke dingen doet",                                    // 28. 
    "emoties onder controle houdt",                         // 29. 
    "zich laat afschrikken",                                // 30. 
    "gezelschap vermijdt",                                  // 31. 
    "eerst aan zichzelf denkt",                             // 32. 
    "iets op het laatste moment doet",                      // 33. 
    "de toekomst donker inziet",                            // 34. 
    "risico's neemt",                                       // 35. 
    "van grote feesten houdt",                              // 36. 
    "ruzie probeert te voorkomen",                          // 37. 
    "op tijd werk afmaakt",                                 // 38. 
    "meteen het ergste vreest",                             // 39. 
    "beslissingen aan anderen overlaat",                    // 40. 
    "graag babbelt",                                        // 41. 
    "ruzie maakt",                                          // 42. 
    "zich gedraagt zoals het hoort",                        // 43. 
    "tegenslag snel opzij zet",                             // 44. 
    "iets van verschillende kanten bekijkt",                // 45. 
    "bang is voor nieuwe ontmoetingen",                     // 46. 
    "rekening houdt met andermans gevoelens",               // 47. 
    "volgens een vast patroon werkt",                       // 48. 
    "tegen een stootje kan",                                // 49. 
    "het initiatief neemt",                                 // 50. 
    "anderen opvrolijkt",                                   // 51. 
    "mensen voor eigen doelen gebruikt",                    // 52. 
    "gevaar opzoekt",                                       // 53. 
    "in tranen uitbarst",                                   // 54. 
    "doet wat anderen willen",                              // 55. 
    "zich afzondert",                                       // 56. 
    "meeleeft met anderen",                                 // 57. 
    "wil dat alles precies klopt",                          // 58. 
    "de dingen van de zonnige kant bekijkt",                // 59. 
    "makkelijk klein is te krijgen",                        // 60. 
    "gesprekken aanknoopt",                                 // 61. 
    "sterke verhalen over zichzelf vertelt",                // 62. 
    "werk laat liggen",                                     // 63. 
    "problemen van zich af kan zetten",                     // 64. 
    "afwacht wat anderen doen",                             // 65. 
    "anderen op afstand houdt",                             // 66. 
    "graag iets voor een ander doet",                       // 67. 
    "de dingen volgens het boekje doet",                    // 68. 
    "zich weet te beheersen",                               // 69. 
    "zich bij de meerderheid aansluit",                     // 70. 
    "liefst alleen is",                                     // 71. 
    "anderen zijn/haar wil oplegt",                         // 72. 
    "hard werkt",                                           // 73. 
    "stevig in zijn/haar schoenen staat",                   // 74. 
    "snel reageert",                                        // 75. 
    "zich gemakkelijk in gezelschap beweegt",               // 76. 
    "de leiding wil hebben",                                // 77. 
    "zich laat afleiden",                                   // 78. 
    "in paniek raakt",                                      // 79. 
    "uitdagingen aangaat",                                  // 80. 
    "in zijn/haar eigen wereldje leeft",                    // 81. 
    "het hoogste woord voert",                              // 82. 
    "afspraken nakomt",                                     // 83. 
    "in de put zit",                                        // 84. 
    "weet wat hij/zij wil",                                 // 85. 
    "dichtklapt bij onbekenden",                            // 86. 
    "andermans mening respecteert",                         // 87. 
    "zich goed voorbereidt",                                // 88. 
    "uit zijn/haar humeur raakt",                           // 89. 
    "gemakkelijk voor de gek is te houden",                 // 90. 
    "makkelijk vrienden maakt",                             // 91. 
    "mensen commandeert",                                   // 92. 
    "onverwachte dingen doet",                              // 93. 
    "ergens over piekert",                                  // 94. 
    "vol met idee&euml;n zit",                              // 95. 
    "vreugde uitstraalt",                                   // 96. 
    "zich in het middelpunt plaatst",                       // 97. 
    "taken verwaarloost",                                   // 98. 
    "het hoofd koel houdt",                                 // 99. 
    "zich in discussies begeeft",                           //100. 
];

