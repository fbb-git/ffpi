function sleep(ms) 
{
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitForReport()
{
   do
   {            
       var http = new XMLHttpRequest();
   
       http.open('HEAD', '$0/$1.pdf', false);
       http.send();
       sleep(1000);
   }
   while (http.status == 404);
}

// application, e.g.,
//
//     De rapportage  wordt op dit moment op basis van de  door u gegeven
//     antwoorden geconstrueerd  (de constructie ervan duurt ongeveer 10
//     seconden).   
// <p>
//     <script> waitForReport(); </script>
// 
//     De rapportage is beschikbaar kan via
//         <a href="$0/$1.pdf" target="_blank">deze link</a>
//     worden bekeken of gedownload.


